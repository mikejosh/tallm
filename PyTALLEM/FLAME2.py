# Simple connector to use FLAME in matlab (b/c it returns a numpy matrix)

from FLAME import FLAME

def FLAME2(ptvec,dim,nn1,nn2):
    nd = len(ptvec)
    numpts = int(nd/dim)
    pts = ptvec.reshape([numpts,dim])
    model = FLAME(cluster_neighbors=nn1,iteration_neighbors=nn2)
    probs = model.fit_predict_proba(pts)
    return probs