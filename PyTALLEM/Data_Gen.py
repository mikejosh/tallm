## Generate or Load Datasets:
## The first four are the examples in (Mike and Perea 2019).
# standard call structure: 
# (pts,dim,labels,ftype,dtype) = DG.name(numpts,namei=vali)

import numpy as np
import os
import sys
from tqdm import tqdm
from scipy.io import loadmat as load

# Platform-Specific File Separator:
sep = os.sep

# CCYL curved cylinder dataset 
def Coil_Cyl(numpts,rad_inn=0.3,spins=2,ranseed=123,hole=0):
    np.random.seed(ranseed)
    def spiral(x,s):
        return np.array([x*np.cos(s*x),np.abs(x)*np.sin(s*x),0])/np.pi
    def dspira(x,s):
        return np.array([np.cos(s*x)-        s*x*np.sin(s*x),
              np.sign(x)*np.sin(s*x)+s*np.abs(x)*np.cos(s*x),0])/np.pi
    def nspira(x,s): # (sgn(x)sin(sx)+s|x|cos(x),
        normal = np.array([np.sign(x)*np.sin(s*x)+s*np.abs(x)*np.cos(s*x),
                                     -np.cos(s*x)+        s*x*np.sin(s*x),0])
        return normal / np.linalg.norm(normal)
        
    def param(a,b):
        pt = spiral(a,spins)
        v1 = nspira(a,spins)
        v2 = np.array([0,0,1])

        return pt + rad_inn*np.cos(b)*v1 + rad_inn*np.sin(b)*v2
    
    dim = 3
    pts=np.zeros([numpts,dim])
    
    if hole:
        As = np.zeros(numpts)
        Bs = np.zeros(numpts)
        index = 0
        while True:
            (a,b) = np.random.uniform(-np.pi,np.pi,2)
            if not (np.abs(a) < hole and np.abs(b) < hole):
                As[index] = a
                Bs[index] = b+np.pi
                index = index+1
                if index >= numpts:
                    break
    else:
        As = np.random.uniform(low=-np.pi,high=np.pi,size=numpts)
        Bs = np.random.uniform(low=0,high=2*np.pi,size=numpts)
    
    labels = np.array([Bs,As]).T
    
    for index in range(numpts):
        pts[index,:] = param(As[index],Bs[index])
    
    ftype = 'conts'
    dtype = 'Euclidean'
    
    return (pts,dim,labels,ftype,dtype)
    
# TLB1 or MOBB dataset: These are image patches made of bars.
def Patches_Mobius(numpts,BW=1,aspect=3,ranseed = 123):
    dim = aspect**2
    
    np.random.seed(ranseed)
    pts = np.empty([numpts,dim])
    
    angles = [np.random.uniform(-np.pi/2,np.pi/2) for x in range(numpts)]
    offset = [np.random.uniform(-BW,BW) for x in range(numpts)]
    
    labels = np.transpose(np.array([angles,offset]))
    angles = np.array(angles)
    offset = np.array(offset)
    
    XYvals = [-1+2*x/(aspect-1) for x in range(aspect)]
    for i in range(dim):
        xval = XYvals[i%aspect]
        yval = XYvals[int(i/aspect)]
        vec1 = [xval,yval]
        for j in range(numpts):
            vec2 = [np.sin(angles[j]),np.cos(angles[j])]
            dist = np.dot(vec1,vec2) - offset[j]
            pts[j,i] = 1/(1+np.exp(dist**2))
    
    ftype = 'conts'
    dtype = 'euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# SBT2 dataset, sum bundle over the torus.
# the "gen" parameter allows the creation of any class of line bundle over any
# dimension of torus; still, the defaults yield the SBT2 dataset. 
def SB_torus(numpts,FS=4,basedim = 2,gen=None,ranseed=123):
    
    np.random.seed(ranseed)
    if type(gen) == type(None):
        gen = np.ones(basedim)
    
    dim = 2*basedim+2
    
    def tor(a):
        pt = []
        for t in a:
            pt.append(np.cos(t))
            pt.append(np.sin(t))
        pt.append(0)
        pt.append(0)
        return np.array(pt)
    def nor(a):
        angle = 0
        for (t,g) in zip(a,gen):
            angle = angle + t*g
        angle = angle/2
        out = np.zeros(dim)
        out[-2] = np.cos(angle)
        out[-1] = np.sin(angle)
        return out
    
    def param(a,x):
        return tor(a) + x*nor(a)
    
    As = np.random.uniform(-np.pi,np.pi,[numpts,basedim])
    Xs = np.random.uniform(-FS/2,FS/2,[numpts,1])
    labels = np.concatenate([As,Xs],axis=1)
    
    pts = np.zeros([numpts,dim])
    for index in range(numpts):
        pts[index,:] = param(As[index,:],Xs[index,0])
    
    ftype = 'conts'
    dtype = 'Euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# TLB2 dataset: 3D pixel patches of planes. (Principal Line bundle over RP2)
def Patches_LBonRP2(numpts,BW=1/2,aspect=3,ranseed=123,isnorm=False):
    dim = aspect**3
    
    np.random.seed(ranseed)
    pts = np.zeros([numpts,dim])
    labels = np.zeros([numpts,4]) # v1, v2, v3 in S^2, offset in [0,1/2]
    
    XYZvals = np.array([-1+2*x/(aspect-1) for x in range(aspect)])
    for slot in range(numpts):
        vec = np.random.multivariate_normal([0,0,0],[[1,0,0],[0,1,0],[0,0,1]])
        vec = vec/np.linalg.norm(vec)
        if vec[2] < 0: # enforce sided coloring.
            vec[2] = -vec[2]
        if isnorm:
            beta = np.random.normal(0,BW/2)
        else:
            beta = np.random.uniform(-BW,BW)
        labels[slot,:-1] = vec[:] # base space coordinates (modulo {+1,-1})
        labels[slot,-1] = beta    # fiber space coordinates (modulo {+1,-1})
        for i in range(aspect):
            for j in range(aspect):
                for k in range(aspect):
                    index = i + aspect*j + (aspect**2)*k
                    pt = XYZvals[[i,j,k]]
                    dist = np.dot(vec,pt) - beta
                    pts[slot,index] = 1/(1+np.exp(dist**2))
    
    ftype = 'conts'
    dtype = 'euclidean'
    
    return (pts,dim,labels,ftype,dtype)
    
# MOBC dataset: Double-Spiral with half twist; 0spins gives classical Mobius strip.
# Spins also add variable density to the problem.
# This one didn't make it into the paper, but its an interesting example.
def mobius_coil(numpts,rad_out=1,rad_inn=0.4,spins=2,ranseed=123,hole=0):
    np.random.seed(ranseed)
    # x in (-np.pi,np.pi)
    def spiral(x,r):
        spir = np.array([x*np.cos(spins*x),np.abs(x)*np.sin(spins*x)])/np.pi
        rota = np.array([[np.cos(r),np.sin(r)],[-np.sin(r),np.cos(r)]])
        return np.matmul(rota,spir)
    
    def param(a,b):
        v1 = np.array([np.cos(a),np.sin(a),0])
        v2 = np.array([0,0,1])
        (s1,s2) = spiral(b,a/2)*rad_inn
        return rad_out*v1 + s1*v1 + s2*v2
        
    dim = 3
    pts=np.zeros([numpts,dim])
    
    if hole:
        As = np.zeros(numpts)
        Bs = np.zeros(numpts)
        index = 0
        while True:
            (a,b) = np.random.uniform(-np.pi,np.pi,2)
            if not (np.abs(a) < hole and np.abs(b) < hole):
                As[index] = a
                Bs[index] = b
                index = index+1
                if index >= numpts:
                    break
    else:
        As = np.random.uniform(low=-np.pi,high=np.pi,size=numpts)
        Bs = np.random.uniform(low=-np.pi,high=np.pi,size=numpts)
    params = np.concatenate([[As],[Bs]],axis=0).T
    
    for index in range(numpts):
        pts[index,:] = param(As[index],Bs[index])
    
    ftype = 'conts'
    dtype = 'Euclidean'
    
    return (pts,dim,params,ftype,dtype)

# PLBC dataset: Image patches made of edges. 
def Patches_Cyl(numpts,BW=1/2,aspect=3,ranseed = 123,n1 = 0,n2 = 0):
    dim = aspect**2
    
    if n1*n2 == 0:
        np.random.seed(ranseed)
        pts = np.empty([numpts,dim])
        
        angles = [np.random.uniform(-np.pi,np.pi) for x in range(numpts)]
        offset = [np.random.uniform(-BW,BW) for x in range(numpts)]
    else:
        numpts = na*no
        pts = np.zeros([numpts,dim])
        
        angles = [2*np.pi*(x%na)/(na-1) for x in range(no*na)]
        offset = list(np.repeat([BW*(2*(x)-(no-1))/(no-1) for x in range(no)],na))
    
    labels = np.array([angles,offset]).T
    angles = np.array(angles)
    offset = np.array(offset)
    
    XYvals = [-1+2*x/(aspect-1) for x in range(aspect)]
    for i in range(dim):
        xval = XYvals[i%aspect]
        yval = XYvals[int(i/aspect)]
        vec1 = [xval,yval]
        for j in range(numpts):
            vec2 = [np.sin(angles[j]),np.cos(angles[j])]
            dist = np.dot(vec1,vec2) - offset[j]
            pts[j,i] = 1/(1+np.exp(dist))
    
    ftype = 'conts'
    dtype = 'euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# KBLB ex for future TALLEM implementation: it requires d=2 and a base map.
def Patches_Klein(numpts,aspect=3,ranseed = 123,n1 = 0, n2 = 0):
    dim = aspect**2
    
    if n1*n2 == 0:
        np.random.seed(ranseed)
        pts = np.empty([numpts,dim])
        
        angle1 = [np.random.uniform(-np.pi/2,np.pi/2) for x in range(numpts)]
        angle2 = [np.random.uniform(-np.pi,np.pi) for x in range(numpts)]
    else:
        numpts = na*no
        pts = np.zeros([numpts,dim])
        
        angle1 = [np.pi*((int(x%na))/(na-1)-1/2) for x in range(no*na)]
        angle2 = [np.pi*((int(x/na))/(na-1)-1/2) for x in range(no*na)]
    
    labels = np.array([angle1,angle2]).T
    angle1 = np.array(angle1)
    angle2 = np.array(angle2)
    
    XYvals = [-1+2*x/(aspect-1) for x in range(aspect)]
    for i in range(dim):
        xval = XYvals[i%aspect]
        yval = XYvals[int(i/aspect)]
        vec1 = [xval,yval]
        for j in range(numpts):
            vec2 = [np.sin(angle1[j]),np.cos(angle1[j])]
            dist = np.dot(vec1,vec2)
            val = (np.cos(angle2[j])*dist + np.sin(angle2[j])*(dist**2 - 1/3))/4
            pts[j,i] = val
    
    ftype = 'conts'
    dtype = 'euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# A simple torus example. 
def torus(numpts,rad_out=1,rad_inn=0.4,ranseed = 123):
    np.random.seed(ranseed)
    
    def param(ao,ro,ai,ri):
        start = [ro*np.cos(ao),ro*np.sin(ao),0]
        shift = [ri*np.cos(ai)*np.cos(ao),ri*np.cos(ai)*np.sin(ao),ri*np.sin(ai)]
        return np.array(start)+np.array(shift)
    dim = 3
    
    if numpts < 0:
        no = int(-numpts*rad_out)
        ni = int(-numpts*rad_inn)
        pts = np.zeros([no*ni,dim])
        labels = np.zeros(no*ni)
        for ii in range(no):
            for jj in range(ni):
                index = ii*ni+jj
                ao = 2*np.pi*ii/no
                ai = 2*np.pi*(((-1)^ii)/2 + jj)/ni
                pts[index] = param(ao,rad_out,ai,rad_inn)
                labels[index] = ao
    else:
        pts=np.zeros([numpts,dim])
        Aos = np.random.uniform(low=0,high=2*np.pi,size=numpts)
        Ais = np.random.uniform(low=0,high=2*np.pi,size=numpts)    
        for index in range(numpts):
            pts[index,:] = param(Aos[index],rad_out,Ais[index],rad_inn)
        labels = Aos

    
    ftype = 'conts'
    dtype = 'Euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# loads mnist digits (same as MATLAB)
def load_mnist(ppc, classes = [8], RF = 1,ranseed = 123):
    np.random.seed(ranseed)
            
    raw = load('Datasets'+sep+'mnist.mat')
    mnist_pts = [raw['mnist_info'][x][0] for x in range(10)]
    nC = length(classes)
    mnist_nums = [len(classes(j)) for j in classes]
    minsize = min(mnist_nums)
    ppc = min(ppc,minsize)
    numpts = ppc*nC;

    if RF > 1 and 28%RF == 0:
        dim = (28/RF)**2;
    else:
        dim = 28**2;
    
    pts = np.zeros([numpts,dim])
    labels = np.zeros(numpts)
    
    for j in range(nC):
        label = classes(j)-1
        labels[j*ppc:(j+1)*ppc] = classes(j)
        ss = np.sort(np.random.choice(mnist_nums(j),size=ppc,replace = False))
        for i in range(ppc):
            k = i + j*ppc
            pts[k,:] = crunch_image(mnist_pts[label][ss(i),:],28,28,RF);
        
    ftype = 'classes'
    dtype = 'euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# loads Brey Faces (same as MATLAB)    
def load_faces(numpts,RF=1,ranseed=123):
    np.random.seed(ranseed)
    raw = load('..'+sep+'Datasets'+sep+'frey_rawface.mat')
    face = raw['ff'].T
    minpts = face.shape[0]
    numpts = min(minpts,numpts)
    if RF > 1 and 4%RF == 0:
       dim = 560/(RF**2)
    else:
        dim = 560
    
    pts = np.zeros([numpts,dim])
    ss = np.sort(np.random.choice(minpts,size=numpts,replace = False))
    for i in range(numpts):
        pts[i,:] = crunch_image(face[ss[i],:],28,20,RF)
    
    labels = ss/max(ss)
    
    ftype = 'conts'
    dtype = 'euclidean'
    
    return (pts,dim,labels,ftype,dtype)

# Helper funtion. RF is the factor for reducing images.
def crunch_image(vec,dim1,dim2,RF):
    if RF > 1 and dim1%RF==0 and mdim2%RF == 0 and len(vec)==dim1*dim2:
        newdim1 = dim1/RF
        newdim2 = dim2/RF
        im = vec.reshape([dim1,dim2]);
        im2 = np.zeros(newdim1,newdim2);
        for ii in range(newdim1):
            for jj in range(newdim2):
                for kk in range(RF):
                    for ll in range(RF):
                        im2[ii,jj] = im2[ii,jj] + im[ii+kk,jj+ll]
        newvec = im2.reshape(newdim1*newdim2)
    else:
        newvec = vec
    return newvec

# Use part 1, sample 1 as a clean start and apply PCA and (rescaled) circular coords
# in TALLEM to present an example which uses TALLEM with real base coords to provide
# a mapping onto the cylinder.
# The dicrete flavor of the dataset comes through...
def load_neuro(part = 1,sample=0):
    folder1 = '../Datasets/Neuro/Dot_Field'
    folder2 = '../Datasets/Neuro/Grating'
    flist1 = os.listdir(folder1)
    flist2 = os.listdir(folder2)
    flist1 =  [f for f in flist1 if f.endswith('.mat')]
    flist2 =  [f for f in flist2 if f.endswith('.mat')]
    raw1 = []
    raw2 = []
    for filename in flist1:
        raw1.append(load(folder1+sep+filename))
    for filename in flist2:
        raw2.append(load(folder2+sep+filename))
    raw = [raw1,raw2][part-1]
    print([flist1,flist2][part][sample])
    portion = raw[sample]
    #SSC = portion['spontaneous_spike_count']) # (no events)
    TSC = portion['trial_spike_count'] # data
    TS = portion['trial_stimulus'] # labels
    data = np.zeros([TSC.shape[1],TSC.shape[0]*TSC.shape[2]])
    for slot in range(TSC.shape[2]):
        data[:,slot*TSC.shape[0]:(slot+1)*TSC.shape[0]] = TSC[:,:,slot].T
    if part == 1:
        labels = np.array([TS[0,0][0],TS[0,0][1]])[:,:,0].T
    else: labels = np.array([TS[0,0][0],TS[0,0][1],TS[0,0][2]]).T
    return (data,labels,portion)

# obtain patches and preprocess:
# (1) normalize with mean & Dnorm.
# (2) take [cutoff] highest contrast.
def patch_and_process(HCnum, aspect=3, n_images=10, cutoff=0.1, ranseed=123):
    # Helper functions for preprocessing:
    im_shape = (1024, 1536)

    def Dnorm(pts):
        dim = pts.shape[1]
        numpts = pts.shape[0]
        aspect = int(np.sqrt(dim))
        if not dim == aspect ** 2:
            print("The points do not represent square images.")
            return
        Dmat = Dmatrix(aspect)
        norms = np.zeros(numpts)
        for index in range(numpts):
            point = pts[index, :]
            norms[index] = np.sqrt(np.matmul(point, np.matmul(Dmat, point)))
        return norms

    def Dmatrix(aspect):
        dim = aspect ** 2
        Dmat = np.zeros([dim, dim])
        # Find the neighbors in the patch and sum differences
        # according to quadratic form sum_{i~j} (x_i-x_j)^2 = xDx
        for i in range(aspect):
            for ii in range(aspect):
                for dir in [(1, 0), (0, 1)]:  # adjacent index
                    j = i + dir[0]
                    jj = ii + dir[1]

                    if (0 <= j < aspect) and (0 <= jj < aspect):
                        # print('('+str(i)+','+str(ii)+')--('+str(j)+','+str(jj)+')')
                        islot = i * aspect + ii
                        jslot = j * aspect + jj
                        Dmat[islot, jslot] = Dmat[islot, jslot] - 1
                        Dmat[jslot, islot] = Dmat[jslot, islot] - 1
                        Dmat[islot, islot] = Dmat[islot, islot] + 1
                        Dmat[jslot, jslot] = Dmat[jslot, jslot] + 1
        return Dmat

    def Dnormalize(pts):
        numpts = pts.shape[0]
        norms = Dnorm(pts)
        for index in range(numpts):
            pts[index, :] = pts[index, :] / norms[index]
        return pts

    def image(i):
        num = '{0:05d}'.format(i)
        filedir = '..' + sep + 'Datasets' + sep + 'Edges' + sep
        path = filedir + 'imk' + num + '.iml'

        with open(path, 'rb') as handle:
            s = handle.read()

        img = np.fromstring(s, dtype='uint16').byteswap()
        img = img.reshape(im_shape)
        img = img[:, 2:-2]  # remove vertical bars on either side (1218).
        return img

    def get_patches(n_patches, shape, n_images=10, ranseed=123):

        images = []
        for index in range(n_images):
            images.append(image(index + 1))

        np.random.seed(ranseed)

        kk = np.random.randint(0, n_images, size=n_patches)
        ii = np.random.randint(0, im_shape[0] - shape[0], size=n_patches)
        jj = np.random.randint(0, im_shape[1] - 4 - shape[1], size=n_patches)  # cutoff

        patches = []
        for [k, i, j] in zip(kk, ii, jj):
            patches.append(images[k][i:i + shape[0], j:j + shape[1]])

        return patches

    numpts = int(HCnum / cutoff)
    patches = get_patches(numpts, [aspect, aspect], n_images=n_images, ranseed=ranseed)
    Dmat = Dmatrix(aspect)
    dim = aspect ** 2

    # log and center: (this can be done easily for 3x3s)
    for index in range(numpts):
        patches[index] = np.log(1 + patches[index])
        patches[index] = patches[index] - np.mean(patches[index])

    # turn into numpy matrix: (but keep the patches for viewing later?)
    # Also,calculate D-norm for each:
    pts = np.zeros([numpts, dim])
    labels = np.zeros(numpts)
    for index in tqdm(range(numpts)):
        patch = patches[index]
        (x, y) = (0, 0)
        pts[index, :] = np.reshape(patch, dim, 'C')
        for i in range(2):
            for j in range(2):
                ii = i - 1
                jj = j - 1
                x = x + ii * patch[ii, jj]
                y = y + jj * patch[ii, jj]
        labels[index] = np.sin(2 * np.arctan2(x, y))

    norms = Dnorm(pts)

    # Take the top percentile High-Contrast points (percentile > cutoff)
    weird_sort = np.argsort(norms)
    HC_subset = weird_sort[int(numpts * (1 - cutoff)):]
    HCpts = pts[HC_subset, :]
    HCpts = Dnormalize(HCpts)
    labels = labels[HC_subset]

    return (HCpts, dim, labels, 'conts', 'euclidean')

# Bulk method for loading molecule lists from XYZ files.
def load_XYZ(filename = 'benzene_old_dft'):
    folder = '..' + sep + 'tallem' + sep + 'Datasets' + sep + 'Chem'
    with open(folder + sep + filename+'.xyz', 'r') as raw:
        reader = csv.reader(raw, delimiter='\t')
        contents = [row for row in reader]
        numlines = len(contents)
        Atoms = []
        for (index,row) in enumerate(contents):
            if len(row) > 1:
                Atoms.append(row[0])
            if index > 2 and len(row) == 1:
                break
        numatoms = index - 2
        numpts = numlines/(numatoms + 2)
        X = np.zeros([numpts,numatoms,3]) # positions
        F = np.zeros([numpts,numatoms,3]) # forces
        E = np.zeros([numpts])   # energy

        for index in range(numpts):
            ii = 1 + numatoms*index
            E[index] = float(contents[ii][0])
            for slot in range(numatoms)
                ii = 2 + numatoms*index + slot
                X[index,atom,0] = float(contents[ii][1])
                X[index,atom,1] = float(contents[ii][2])
                X[index,atom,2] = float(contents[ii][3])
                F[index,atom,0] = float(contents[ii][4])
                F[index,atom,1] = float(contents[ii][5])
                F[index,atom,2] = float(contents[ii][6])