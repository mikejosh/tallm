## script for comparing offset usage for various datasets.

# Typical stuff:
from tqdm import tqdm
import random
import numpy as np
import time
import os.path
import copy
import sys

# For plotting and showing stuff:
import matplotlib.pyplot as plt

# Toolkits with personal API:
import Data_Gen as DG
import Data_Rep as DR
import Class_Map as CM
import Fiber_Bundles as FB

# System specific file separator.
sep = os.sep

# Method used to determine the base mapping. If you don't have Ripser, use 'given'.
# 'cohomplus' uses persistent homology on nbhd geodesic distances and is used to
# create the images and values in (Mike and Perea 2019). 
baseType = 'cohomplus'
#baseType = 'cohom'
#baseType = 'linear'
#baseType = 'given'

baseP = {}
baseP['baseFinder'] = baseType
localP = {}

# Simple presentations for the cylinder and mobius strip.
# As amd Hs are the "Angles" and "Heights" (base and fiber coords)
def cyl(As,Hs):
    return np.array([[np.cos(a),np.sin(a),h] for (a,h) in zip(As,Hs)])
def mob(As,Hs):
    return np.array([[(1+h*np.cos(a/2))*np.cos(a),
    (1+h*np.cos(a/2))*np.sin(a),h*np.sin(a/2)]
        for (a,h) in zip(As,Hs)])
def frm(As,Hs,frame):
    return np.array([[(1+h*f[0])*np.cos(a),(1+h*f[0])*np.sin(a),h*f[1]]
        for (a,h,f) in zip(As,Hs,frame)])

# SHARED DATASET PARAMETERS
numpts = 2000   # number of data points
stage = 'final'
dataType = sys.argv[1]
# UNIQUE DATASET PARAMETERS
if dataType == 'PLBC':
    RI = 0.4
    localP['numcov']=64  
elif dataType == 'TLB1': 
    RI = 0.4
elif dataType == 'CCYL': 
    RI = 0.2
elif dataType == 'MOBC': 
    numpts = 10000
    RI = 0.4
    localP['numcov']=32

# prepare parameters for TALLEM
baseP['baseType'] = 'T1'
baseP['basedim'] = 1
if dataType == 'PLBC': # Trivial line bundle over circle (cylinder)
    (pts,dim,params,ftype,dtype) = DG.Patches_Cyl(numpts,BW=RI,aspect=5)
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
if dataType == 'CCYL': # Cylinder Coil
    (pts,dim,params,ftype,dtype) = DG.Coil_Cyl(numpts,rad_inn=RI,spins=0.5)
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
if dataType == 'TLB1': # Tautological Line bundle over RP1 (mobius band)
    (pts,dim,params,ftype,dtype) = DG.Patches_Mobius(numpts,BW=RI,aspect=5)
    baseP['baseCoords'] = np.array([[np.cos(2*a),np.sin(2*a)] for a in params[:,0]])
if dataType == 'MOBC': # mobius coil
    (pts,dim,params,ftype,dtype) = DG.mobius_coil(numpts,rad_inn = RI,spins=1.5)
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
color1 = DR.colorprep(params[:,0],symm=False)
color2 = DR.colorprep(params[:,1],symm=False)
Cvec = params[:,1]

    # Compare RMSE before and after offsets are included.
    # It may also be more reasonable to only include cover2...
    #(cocycle1,shifts0_1,orths1,RMSE1) = CM.line_align(pts,cover1,models0_1)
    #(cocycle1,shifts1_1,orths1,RMSE1) = CM.line_align(pts,cover1,models1_1)
    #(cocycle2,shifts0_2,orths2,RMSE2) = CM.line_align(pts,cover2,models0_2)
    #(cocycle2,shifts1_2,orths2,RMSE2) = CM.line_align(pts,cover2,models1_2)

    # Determine change in shifting sizes, ie distances from optimal alignments.
    #numkey1 = len(shifts0_1)
    #numkey2 = len(shifts0_2)
    #Err0_1 = np.zeros(numkey1)
    #Err1_1 = np.zeros(numkey1)
    #Err0_2 = np.zeros(numkey2)
    #Err1_2 = np.zeros(numkey2)

    #for (index,key) in enumerate(shifts0_1):
    #    Err0_1[index] = (shifts0_1[key][0])**2
    #    Err1_1[index] = (shifts1_1[key][0])**2

    #for (index,key) in enumerate(shifts0_2):
    #    Err0_2[index] = (shifts0_2[key][0])**2
    #    Err1_2[index] = (shifts1_2[key][0])**2

    #rmse0_1 = np.sqrt(np.mean(Err0_1))
    #rmse1_1 = np.sqrt(np.mean(Err1_1))
    #rmse0_2 = np.sqrt(np.mean(Err0_2))
    #rmse1_2 = np.sqrt(np.mean(Err1_2))

    # Report Errors.
    #print('')
    #print((np.sqrt(np.mean(RMSE1**2)),rmse0_1,rmse1_1))
    #print((np.sqrt(np.mean(RMSE2**2)),rmse0_2,rmse1_2))
    #print('')
    #print((np.sqrt(np.max(Err0_1)),np.sqrt(np.max(Err1_1)) ))
    #print((np.sqrt(np.max(Err0_2)),np.sqrt(np.max(Err1_2)) ))

None

### ---------- First do TALLEM coordinates: ---------- ###
FBI = FB.coords(pts,baseParams=baseP,localParams=localP,stage=stage,
    MV = 0,Cvec=Cvec)

offs = FBI['offsets']
models1 = FBI['models']
cover1 = FBI['cover1']
cover2 = FBI['cover2']
assign = FBI['assign2']
cocycle = FBI['cocycle']

for doof in [-2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2]:
    # restrict models to cover2, then offset them.
    models2 = []
    for (vec, set1, set2) in zip(models1, cover1, cover2):
        subset = np.nonzero(np.in1d(set1, set2))[0]
        models2.append(vec[subset])
    # corrected to: offs = -delta0^+(shifts), bc of how we defined delta0?
    models2_off = []
    for (vec, off) in zip(models2, offs):
        models2_off.append(vec + doof * off)
    # non-weighted variance in coordinates
    SEVs = np.zeros([numpts])
    ranges = np.zeros([numpts])
    for (index,pt) in enumerate(pts):
        spots = [np.where(cover2[v] == index)[0][0] for v in assign[index]]
        v0 = assign[index][0]
        vals = np.array([cocycle[v0,v]*models2_off[v][spot]
                         for (v,spot) in zip(assign[index],spots)])
        SEVs[index] = np.var(vals)
        ranges[index] = np.max(vals) - np.min(vals)
    #print(str(doof) + ":  " + str(np.sqrt(np.mean(SEVs))))
    print(str(doof) + ":  " + str(np.mean(ranges)))