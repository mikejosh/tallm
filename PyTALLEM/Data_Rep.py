# Aliased as "Data Represenation" (Also DR)
# This file includes methods for dim reduction, plotting, and clustering, etc.
# None of these methods should be learning from labels, but all of them seek
# to change the representation of the data directly. This does not include NL.

# basic imports
import random
import os
import numpy as np
import networkx as nx
import networkx.drawing.layout as NDL
import time
from tqdm import tqdm

# sklearn imports
from sklearn.decomposition import PCA
from sklearn import manifold
from sklearn.neighbors import NearestNeighbors as KNN
from sklearn.neighbors import KernelDensity as KDE
from sklearn.metrics.pairwise import pairwise_distances as PWD

# scipy imports
from scipy.cluster import hierarchy
from scipy.spatial.distance import squareform as Dmat_Convert

# matplotlib imports
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from matplotlib import offsetbox
from matplotlib import collections

# exterior imports(DL from conda)
import umap as unimap
from pydiffmap import diffusion_map as diffmap

sep = os.sep

## Set functions on lists ##
def intersect(a, b):
    return list(set(a) & set(b))
def union(a, b):
    return list(set(a) | set(b))
def setdiff(a, b):
    return list(set(a)-set(b))
def unique(a):
    return list(set(a))
    
### PLOTTING TOOLS
# Color palette from interval values. Large range of hues. 
def colorprep(vec,BW = 0.8,R=1/2,shift=0.1,symm=False):
    if symm:
        top = max(abs(vec))
        bot = -top
    else:
        top = max(vec)
        bot = min(vec)
    v1 = np.array([1/3,-2/3,1/3])
    v2 = np.array([1/2,0,-1/2])
    v1 = v1/np.linalg.norm(v1) #sqrt(2/3)
    v2 = v2/np.linalg.norm(v2)
    b = np.array([1/2,1/2,1/2])    
    pc = [BW*(x-bot)/(top-bot) for x in vec]
    
    def param(x):
        (cx,sx) = (np.cos(2*np.pi*(x+shift)),np.sin(2*np.pi*(x+shift)))
        return b + R*(cx*v1 + sx*v2)
    
    colors = np.array([param(x) for x in pc])
    
    return colors
    
# Color palette from circular values. 
def circle_color(vec,period = 2*np.pi,v1 = np.array([1,0,0]),v2 = np.array([0,1,0])):
    pc = [(x%period)/period for x in vec]
    v1 = v1/np.linalg.norm(v1)
    v2 = v2 - np.sum(v1*v2)*v1
    v2 = v2/np.linalg.norm(v2)
    
    Cs = [(np.cos(2*np.pi*x)+1)/2 for x in pc]
    Ss = [(np.sin(2*np.pi*x)+1)/2 for x in pc]
    
    R = [Cs[i]*v1[0] + Ss[i]*v2[0] for i in range(len(pc))]
    G = [Cs[i]*v1[1] + Ss[i]*v2[1] for i in range(len(pc))]
    B = [Cs[i]*v1[2] + Ss[i]*v2[2] for i in range(len(pc))]

    colors = np.array([R,G,B]).T
    return colors

# Color palette from sphere coords
def RP2_color(vec):  # conts color scheme on RP2 (from stereos)
    def param(x):
        return

    return np.array([param(x) for x in pc])

# Color palette from RP2 values. (steregraphic coords)
# default is "rainbow" with variable darkness. (light purple/dark green)
def RP2_color(vec,v1=[1,0,0],v2=[0,0,1]): # conts color scheme on RP2 (from stereos)
    b = np.array([1/2,1/2,1/2])
    v1 = v1/np.linalg.norm(v1) #sqrt(2/3)
    v2 = v2/np.linalg.norm(v2) #sqrt(2/3)
    pc = np.array([[np.arctan2(x1,x2),(x1**2+x2**2)**(2/3)] for (x1,x2) in vec])
    
    def param(x,n):
        (cx,sx) = (np.cos(2*x),np.sin(2*x))
        return (1-n)*b + n*(2*b+cx*v1+sx*v2)/2
    
    return np.array([param(x,n) for (x,n) in pc])
    
# Monochrome palette, generates perpendicular to RP2 color.
def monocolor(vec,BW = 1,symm=False,v1 = np.array([0,1,0])):
    if symm:
        top = max(abs(vec))
        bot = -top
    else:
        top = max(vec)
        bot = min(vec)
    
    v1 = v1/np.linalg.norm(v1) #sqrt(2/3)
    b = np.array([1/2,1/2,1/2])    
    pc = [(x-bot)/(top-bot) for x in vec]
    
    def param(x):
        return b + BW*(x-1/2)*v1
    
    return np.array([param(x) for x in pc])
    
# Color palette in torus. One mode is hue, the other is darkness/saturation. 
def T2_color(vec,R=1/3,r=1/6): # conts color scheme on T2. (from [-pi,pi]^2)
    v1 = np.array([1/3,-2/3,1/3])
    v2 = np.array([1/2,0,-1/2])
    v1 = v1/np.linalg.norm(v1) #sqrt(2/3)
    v2 = v2/np.linalg.norm(v2)
    b = np.array([1/2,1/2,1/2])
    bn = b/np.linalg.norm(b) # binormal for the focus circle
    
    def param(x,y):
        (cx,sx,cy,sy) = (np.cos(x),np.sin(x),np.cos(y),np.sin(y))
        nor = cx*v1 + sx*v2
        return b + R*nor + r*(cy*nor+sy*bn)
    
    colors = np.array([param(x,y) for (x,y) in zip(vec[:,0],vec[:,1])])
    
    return colors
    
# Color palette on a 2-simplex from 2 out of 3 barycentric coords.
def two_bary_color(barys,v1 = [1,0,0],v2 = [0,0,1]):
    v1 = v1/(2*np.linalg.norm(v1))
    v2 = v2/(2*np.linalg.norm(v2))
    R = [v1[0]*x[0]+v2[0]*x[1]+1/2 for x in barys]
    G = [v1[1]*x[0]+v2[1]*x[1]+1/2 for x in barys]
    B = [v1[2]*x[0]+v2[2]*x[1]+1/2 for x in barys]
    colors = np.array([R,G,B]).T
    return colors

# standard call for 3D scatterplot. ee/rr give initial view.
def scatter3(X,colors = 'r',ee=45,rr = 45,RF = False):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = [X[i,0] for i in range(X.shape[0])]
    y = [X[i,1] for i in range(X.shape[0])]
    z = [X[i,2] for i in range(X.shape[0])]

    ax.scatter(x, y, z, c=colors, marker='o')

    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')
    ax.view_init(elev=ee,azim=rr)
    
    if RF:
        return (fig,ax)
    else:
        return ax

# standard call for 2D scatterplot. Options to force aspect ratio.
def scatter2(X,colors = 'r', ax = None, equalize = False,RF = False):
    if ax == None:
        fig = plt.figure()
        if equalize:
            ax = fig.add_subplot(111, aspect='equal')
        else:
            ax = fig.add_subplot(111)

    x = [X[i,0] for i in range(X.shape[0])]
    y = [X[i,1] for i in range(X.shape[0])]

    xmin = np.min(X[:,0])
    xmax = np.max(X[:,0])
    xbon = (xmax-xmin)/40
    xmin = xmin - xbon
    xmax = xmax + xbon
    
    ymin = np.min(X[:,1])
    ymax = np.max(X[:,1])
    ybon = (ymax-ymin)/40
    ymin = ymin - ybon
    ymax = ymax + ybon

    ax.set_xlim(xmin,xmax)
    ax.set_ylim(ymin,ymax)
    
    ax.scatter(x, y, c=colors, marker='o')
    if RF:
        return (fig,ax)
    else:
        return ax
    
# Variable call for scatterplot. Accepts 1D, 2D, or 3D input. 
def scatterV(X,colors='r'):
    if len(X.shape) == 1:
        dim = 1
    else:
        dim = X.shape[1]
    if dim == 1:
        X = np.array([[x,x] for x in X])
        dim = 2
    if dim == 2:
        return scatter2(X,colors=colors)
    else:
        return scatter3(X,colors=colors)

# Converts a Matplotlib figure to a numpy array. 
def fig2data(fig):
    # draw the renderer
    fig.canvas.draw()
    # Get the RGB buffer from the figure
    buf = fig.canvas.tostring_rgb()
    w,h = fig.canvas.get_width_height()
    # read and reshape
    return np.fromstring(buf,dtype=np.uint8).reshape(h,w,3)

# plots a collection of planes within the standard cube [-1,1]^3.
def plot_plane(norms,pts,alpha = 0.5,ee=45,rr=45,RF=False):
    # first, determine tangent vectors.
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    colorvec = monocolor(list(range(len(norms))),symm=False)
    ax.scatter(np.array([1,-1]),np.array([1,-1]),np.array([1,-1]))
    
    for (norm,pt,color) in zip(norms,pts,colorvec):
        norm = norm/np.linalg.norm(norm)
        if np.abs(norm[0]) < 10**(-6) and np.abs(norm[1]) < 10**(-6):
            norm = np.array([0,0,1])
            tan1 = np.array([1,0,0])
            tan2 = np.array([0,1,0])
        else:
            tan1 = np.array([norm[1],-norm[0],0])
            tan1 = tan1/np.linalg.norm(tan1)
            tan2 = np.cross(norm,tan1)
                
        # can we find the corners?
        # norm.(x,y,z) = norm.pt
        d = np.dot(norm,pt)
        corners = []
        for (aj,ak) in [[0,1],[0,2],[1,2]]:
            ai = setdiff([0,1,2],[aj,ak])
            for (sj,sk) in [[-1,-1],[-1,1],[1,-1],[1,1]]:
                numer = (d-sj*norm[aj]-sk*norm[ak])
                if norm[ai] == 0:
                    if numer == 0:
                        next = np.zeros(3)
                        next[aj]=sj
                        next[ak]=sk
                        next[ai]=1
                        corners.append((next[0],next[1],next[2]))
                        next = np.zeros(3)
                        next[aj]=sj
                        next[ak]=sk
                        next[ai]=-1
                        corners.append((next[0],next[1],next[2]))
                    continue
                next = np.zeros(3)
                next[aj]=sj
                next[ak]=sk
                next[ai]=numer/norm[ai]
                if np.abs(next[ai]) <= 1:
                    corners.append((next[0],next[1],next[2]))
        corners = np.array([np.array(x) for x in unique(corners)])
        Ss = [np.dot(x,tan1)/np.linalg.norm(x) for x in corners]
        Ts = [np.dot(x,tan2)/np.linalg.norm(x) for x in corners]
            
        center = d*norm
        angles = np.arctan2(Ss,Ts)
        order = list(np.argsort(angles))
        order = order + [order[0]] #(reiterate the initial point)
        
        points = np.array([np.array([corners[x],center]) for x in order])
        xx = points[:,:,0]
        yy = points[:,:,1]
        zz = points[:,:,2]
        
        ax.plot_surface(xx,yy,zz,alpha=alpha,color=color)
    ax.view_init(elev=ee,azim=rr)
    if RF:
        plt.axis('off')
        return fig2data(fig)
    else:
        plt.show()
        return
    
# plots a graph from vertex locations and edge information. 
# Used to plot spanning trees for AGA-TALLEM.
def edge_scatter(X,edges,color1 = 'r', color2 = 'b', ax = None):
    if ax == None:
        fig,ax = plt.subplots()
    
    x = [X[i,0] for i in range(X.shape[0])]
    if X.shape[1] == 1:
        y = x
    else:
        y = [X[i,1] for i in range(X.shape[0])]

    ax.scatter(x, y, c=color1, marker='o')
    
    lines = []
    for edge in edges:
        lines.append([(x[edge[0]],y[edge[0]]),(x[edge[1]],y[edge[1]])])
    lc = collections.LineCollection(lines, colors = color2, linewidths=2)
    
    ax.add_collection(lc)
    
    return ax

# Generates a grid of scatterplots comparing the columns of X and Y.
def multi_scatter(X,Y,colors = 'r'):
    dim1 = X.shape[1]
    dim2 = Y.shape[1]
    fig,ax = plt.subplots(dim1,dim2)
    for i in range(dim1):
        x = X[:,i]
        for j in range(dim2):
            y = Y[:,j]
            ax[i,j].scatter(x,y,c=colors)
    return ax

# scatter plot of images at locations X, with size (zoom).
def imscatter(X, images, ax=None, zoom=1):
    if ax is None:
        fig,ax = plt.subplots()

    x = X[:,0]
    y = X[:,1]
    for (index, (x0, y0)) in enumerate(zip(x, y)):
        im = OffsetImage(images[index], zoom=zoom, cmap='gray')
        ab = AnnotationBbox(im, (x0, y0), xycoords='data', frameon=False)
        ax.add_artist(ab)
    ax.update_datalim(np.column_stack([x, y]))
    ax.autoscale()

    return ax

# Call for 3d scatter plot of images.
# Detailed code for plotting...
def imscatter3(X, images, fig=None, ax=None, zoom=1):
    # accept fig, ax from scatter3; otherwise:
    if (ax is None) or (fig is None):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')

    xs = [X[i, 0] for i in range(X.shape[0])]
    ys = [X[i, 1] for i in range(X.shape[0])]
    zs = [X[i, 2] for i in range(X.shape[0])]
    ax.scatter(xs,ys,zs,marker="o")

    # Create a dummy 2D axes to place annotations to
    ax2 = fig.add_subplot(111, frame_on=False)
    ax2.axis("off")
    ax2.axis([0, 1, 0, 1])

    # Class used to obtain image annotations.
    class ImageAnnotations3D():
        def __init__(self, xyz, imgs, ax3d, ax2d):
            self.xyz = xyz
            self.imgs = imgs
            self.ax3d = ax3d
            self.ax2d = ax2d
            self.annot = []
            for s, im in zip(self.xyz, self.imgs):
                x, y = self.proj(s)
                self.annot.append(self.image(im,[x,y]))
            self.lim = self.ax3d.get_w_lims()
            self.rot = self.ax3d.get_proj()
            self.cid = self.ax3d.figure.canvas.mpl_connect("draw_event", self.update)

            self.funcmap = {"button_press_event": self.ax3d._button_press,
                            "motion_notify_event": self.ax3d._on_move,
                            "button_release_event": self.ax3d._button_release}

            self.cfs = [self.ax3d.figure.canvas.mpl_connect(kind, self.cb) \
                        for kind in self.funcmap.keys()]

        def cb(self, event):
            event.inaxes = self.ax3d
            self.funcmap[event.name](event)

        def proj(self, pt):
            """ From a 3D point in axes ax1,
                calculate position in 2D in ax2 """
            x3, y3, z3 = pt # only renames here to avoid "X".
            x2, y2, _ = proj3d.proj_transform(x3, y3, z3, self.ax3d.get_proj())
            tr = self.ax3d.transData.transform((x2, y2))
            return self.ax2d.transData.inverted().transform(tr)

        def image(self, arr, xy):
            """ Place an image (arr) as annotation at position xy """
            im = offsetbox.OffsetImage(arr, zoom=zoom, cmap = 'Greys')
            im.image.axes = ax
            ab = offsetbox.AnnotationBbox(im, xy, xybox=(-0., 0.),
                              xycoords='data', boxcoords="offset points",
                              pad=0,frameon=False)
#            ab = offsetbox.AnnotationBbox(im, xy, # changed.
#                                          xycoords='data',
#                                          frameon=False)
            self.ax2d.add_artist(ab)
            return ab

        def update(self, event):
            if np.any(self.ax3d.get_w_lims() != self.lim) or \
                    np.any(self.ax3d.get_proj() != self.rot):
                self.lim = self.ax3d.get_w_lims()
                self.rot = self.ax3d.get_proj()
                for s, ab in zip(self.xyz, self.annot):
                    ab.xy = self.proj(s)

    IA = ImageAnnotations3D(np.c_[xs,ys,zs],images,ax,ax2)
    plt.show()
    return (ax,ax2)

### Assorted Data Reduction Methods for comparison and base/local maps.
def isomap(X,outdim,nn=15):
    model = manifold.Isomap(n_neighbors=nn,n_components=outdim)
    return model.fit_transform(X)
def lle(X,outdim,nn=15,reg=0.001):
    model = manifold.LocallyLinearEmbedding(n_neighbors=nn,n_components=outdim,
        reg=reg,method='modified')
    Xnew = model.fit_transform(X)
    return pca(Xnew)
def lapem(X,outdim,nn=15,metric='euclidean'):
    #A = KNN_Graph(X,Y = None,nn=nn,mode='distance',metric=metric,toarr=True)
    model = manifold.SpectralEmbedding(n_components=outdim,
        affinity='nearest_neighbors')
    return model.fit_transform(X)
def mds(X,outdim):
    model = manifold.MDS(n_components=outdim,max_iter=250)
    Xnew = model.fit_transform(X)
    return pca(Xnew)
def tsne(X,outdim,middim=0):
    (numpts,dim) = X.shape
    if middim == 0:
        middim = dim
    Xpca = pca(X,middim)
    if outdim <=3:
        dr = manifold.TSNE(n_components = outdim, verbose = 1)
    else:
        dr = manifold.TSNE(n_components = outdim, method = 'exact', verbose = 1)
    Xnew = dr.fit_transform(X)
    return Xnew
def umap(X,outdim,nn=50,min_dist=0.5,metric='euclidean'):
    model = unimap.UMAP(n_neighbors=nn,n_components=outdim,min_dist=min_dist,
        metric=metric)
    Xnew = model.fit_transform(X)
    return pca(Xnew)
def dmap(X,outdim,nn=400):
    model = diffmap.DiffusionMap.from_sklearn(n_evecs=outdim,k=nn,epsilon='bgh',alpha=1.0)
    # fit to data and return the diffusion map.
    return model.fit_transform(X)

# Classical multi-dimensional scaling.
def cmds(Dmat,outdim,type=1):
    indim = len(Dmat)
    if outdim >= indim:
        print('asking dimension is too big. reducing.')
        outdim = indim - 1
    Dsqd = Dmat*Dmat

    # define the vectors to build the gram matrix B:
    Dvec = np.matmul(Dsqd,np.ones([indim,1]))
    B = np.empty([indim,indim])
    Dsum = np.sum(Dvec)
    # Define the gram matrix B:
    for i in range(indim):
        for j in range(indim):
            B[i,j] = (-1/2)*(Dsqd[i,j]-(Dvec[i]+Dvec[j])/indim+Dsum/(indim**2))

    (val,vec) = np.linalg.eig(B)
    sort = np.argsort(val)[::-1]

    val = val[sort]
    reals = val > 0
    val = val[reals]
    lam2 = np.diag(val)
    lam = np.sqrt(lam2)
    vec = vec[:,sort]
    vec = vec[:,reals]
    X = np.matmul(vec,lam)

    Xnew = pca(X,outdim)

    if type == 1:
        return Xnew
    if type == 2:
        Xinv = np.linalg.pinv(Xnew)
        return (Xnew,Xinv)

# principal components analysis; option to output more info.
def pca(X,outdim=0,verbose = False):
    if outdim == 0:
        outdim = X.shape[1]
    pca = PCA(n_components=outdim)
    Xpca = pca.fit_transform(X)
    if verbose:
        vars = pca.explained_variance_
        comp = pca.components_
        mean = pca.mean_
        return (Xpca,vars,comp,mean)
    return Xpca
# repeat the PCA mapping:
def repeat_pca(Y,comp,mean):
    (numpts,dim) = Y.shape
    mm = np.repeat(np.array([mean]),numpts,axis=0)
    Ypca = Y - mm
    Ypca = np.matmul(Ypca,np.transpose(comp))
    return Ypca
# "undo" the PCA mapping:
def undo_pca(Xpca,comp,mean):
    (numpts,dim) = Xpca.shape
    mm = np.repeat(np.array([mean]),numpts,axis=0)
    X = np.matmul(Xpca,comp)
    X = X + mm
    return X
# Centered Variant of PCA which forces mean = 0 (SVD)
def cpca(X,outdim=0,verbose = False):
    if outdim == 0:
        outdim = X.shape[1]
    numpts = X.shape[0]
    XXT = np.matmul(X.T,X) # dim x dim 'covariance mat'
    (vars,comp) = np.linalg.eigh(XXT/numpts)
    sort = np.argsort(vars)[::-1] # largest to smallest
    vars = vars[sort[:outdim]]
    comp = comp[:, sort[:outdim]] # dim x outdim
    Xpca = np.matmul(X,comp)
    if verbose:
        mean = np.zeros(outdim)
        return (Xpca,vars,comp,mean)
    return Xpca

# Alternative kernel functions (RBFs/bumps): (may be used for PoU)
def gausskern(a,b,h=1):
    return np.exp(-(np.linalg.norm(a-b)**2)/(2*h**2))
def RQkern(a,b,h=1):   # heavy tail
    return (2*h**2)/(np.linalg.norm(a-b)**2 + 2*h**2)
def expkern(a,b,h=1):  # heavier tail
    return np.exp(-np.linalg.norm(a-b)/h)
def IQkern(a,b,h=1,off=1): # VERY HEAVY TAIL
    return 1/np.sqrt(np.linalg.norm(a-b)**2/(h**2)+off**2)

# Outputs adjacency matrix for the kNN graph with k = nn.
# first coordinate indicates member of Y ie, out[i,:] gives NNs of Y[i,:]
# second coordinate indicates member of X, out[:,j] shows that X[j,:] is an NN.
# modes are 'connectivity' and 'distance'
def KNN_Graph(X,Y = None,nn=1,mode='connectivity',metric='euclidean',toarr=True):
    if type(Y) == type(None):
        Y = X
    NNsearch = KNN(n_neighbors=nn,n_jobs=-1,metric=metric)
    NNsearch.fit(X)
    Adj = NNsearch.kneighbors_graph(Y,mode=mode)
    if toarr:
        return Adj.toarray()
    else:
        return Adj

# Simple call for kernel density estimation for real-valued vector. 
def simple_KDE(vec,numvals=1000,BWratio=10,kernel='gaussian'):
    minv = np.min(vec)
    maxv = np.max(vec)
    breadth = (maxv-minv)
    min2 = minv - breadth/10
    max2 = maxv + breadth/10
    BW = breadth/BWratio
    kde = KDE(kernel=kernel,bandwidth=BW).fit(vec.reshape([len(vec),1]))
    ins = np.linspace(min2,max2,numvals)[:,np.newaxis]
    outs = np.exp(kde.score_samples(ins))[:,np.newaxis]
    return np.concatenate([ins,outs],axis=1)
        
### Functions used by Deformation Retraction ###

# Simple Mean-shift algorithm.
def mean_shift(pts,k=10,QQ=0.05,nBW=1000,ratio = 1,sphere=False):
    (numpts, dim) = pts.shape
    X = np.zeros([numpts, dim])
    SSbw = np.random.choice(numpts, size=nBW)
    Dmat = PWD(pts[SSbw, :], metric='euclidean')
    dists = Dmat.flatten('C')
    BW = np.percentile(dists, 100 * QQ)
    model = KNN(k + 1)
    model.fit(pts)
    nbhds = model.kneighbors(pts, return_distance=False)
    for i in tqdm(range(numpts)):
        pi = X[i, :]
        pn = np.zeros(dim)
        nbhd = nbhds[i][1:]
        wgts = np.zeros(k)
        cloud = pts[nbhd, :]
        for (j, pj) in enumerate(cloud):
            wgts[j] = gausskern(pi, pj, h=BW)
            pn = pn + wgts[j] * pj
        pn = pn / np.sum(wgts)
        diff = pn - pi
        X[i,:] = pi + diff / ratio
        if sphere:
            X[i,:] = X[i,:]/np.linalg.norm(X[i,:])
    return X

def MS_repeat(pts,k=10,QQ=0.05,nBW=1000,ratio=1,iters = 2,sphere = False):
    X = np.zeros(pts.shape)
    X[:] = pts[:]
    for index in range(iters):
        Xnew = mean_shift(X,k=k,QQ=QQ,nBW=nBW,ratio=ratio,sphere=sphere)
        X[:] = Xnew[:]
    return X

# Manifold Blurring Mean-Shift (MBMS: Wang and Carreira-Perpinan)
# Uses local tangent space estimates to guide mean-shift.
# clustering is used to separate nearby surface patches. 
def MBMS(pts,nn=10,QQ = 0.05,idim=2,max_iters=3,ratio=10,
        metric='euclidean',nBW=1000,doclust=False,verbose=False):
    (numpts,dim) = pts.shape    
    X = np.zeros([numpts,dim])
    X = pts[:,:]
    for iter in range(max_iters):
        # determine bandwidth:
        SSbw = np.random.choice(numpts,size=nBW)
        Dmat = PWD(X[SSbw,:],metric=metric)
        dists = Dmat.flatten('C')
        BW = np.percentile(dists,100*QQ)
        print('The bandwidth is '+str(BW))
        model = KNN(nn+1)
        model.fit(X)
        
        Xnew = np.zeros([numpts,dim])
        nbhds = model.kneighbors(X,return_distance = False)
        for i in tqdm(range(numpts)):
            pi = X[i,:]
            pn = np.zeros(dim)
            if doclust: # check for split neighbors might need improved?
                nbhd = nbhds[i]
                Dmat = PWD(X[nbhd,:])
                Dmat = (Dmat+Dmat.T)/2
                Dvec = Dmat_Convert(Dmat,force='tovector')
                clusting = hierarchy.linkage(Dvec,method='single')
                pair = clusting[-2:,2]
                if pair[1]/pair[0] > 1.5: # (clearly seps into 2 pieces)
                    clab = hierarchy.cut_tree(clusting,n_clusters=2)[:,0]
                    c0 = np.where(clab == 0)[0]
                    nbhd = nbhds[i][c0][1:]
                else:
                    nbhd = nbhds[i][1:]
            else:
                nbhd = nbhds[i][1:]
            
            wgts = np.zeros(nn)
            cloud = X[nbhd,:]
            (no,vars,comp,no) = pca(cloud,idim+2,verbose=True)

            for (j,pj) in enumerate(cloud):
                wgts[j] = gausskern(pi,pj,h=BW)
                pn = pn+wgts[j]*pj
            pn = pn/np.sum(wgts)
            diff = pn - pi # pn = pi + diff
                        
            for dir in range(idim):
                vec = comp[dir:dir+1,:]
                mat = np.matmul(vec.T,vec)
                diff = diff - np.matmul(mat,diff)
            Xnew[i,:] = pi + diff/ratio
        if verbose:
            print("Example variations: " + str(vars))
        X[:,:] = Xnew[:,:]
    return X
    
# Aymmetric MBMS only projects pts onto fixed Y.
# Ratios performs the shift in multiple steps; should increase to 1.
def MBMS2(pts,Y,nn=50,BWR=1,idim=2,ratios = [1],
        doclust=False,verbose=False):
    (numpts,dim) = pts.shape
    X = np.zeros([numpts,dim])
    X = pts[:,:]
    for (iter,ratio) in enumerate(ratios):
        Xnew = np.zeros([numpts,dim])
        model = KNN(nn)
        model.fit(Y)
        (dists,nbhds) = model.kneighbors(X,return_distance = True)
        BW = BWR*np.mean(dists)
        print('The bandwidth is '+str(BW))
        
        for i in range(numpts):
            pi = X[i,:]
            pn = np.zeros(dim)
            if doclust: # check for split neighbors might need improved?
                nbhd = nbhds[i]
                Dmat = PWD(Y[nbhd,:])
                Dmat = (Dmat+Dmat.T)/2
                Dvec = Dmat_Convert(Dmat,force='tovector')
                clusting = hierarchy.linkage(Dvec,method='single')
                pair = clusting[-2:,2]
                if pair[1]/pair[0] > 1.5: # (clearly seps into 2 pieces)
                    clab = hierarchy.cut_tree(clusting,n_clusters=2)[:,0]
                    c0 = np.where(clab == 0)[0]
                    c1 = np.where(clab == 1)[0]
                    l0 = len(c0)
                    l1 = len(c1)
                    cc = [c0,c1][np.argmax([l0,l1])] # chosen cluster
                    nbhd = nbhds[i][cc]
                else:
                    nbhd = nbhds[i]
            else:
                nbhd = nbhds[i]
            
            wgts = np.zeros(nn)
            cloud = Y[nbhd,:]
            (no,vars,comp,no) = pca(cloud,idim+2,verbose=True)

            for (j,pj) in enumerate(cloud):
                wgts[j] = gausskern(pi,pj,h=BW)
                pn = pn+wgts[j]*pj
            pn = pn/np.sum(wgts)
            diff = pn - pi # pn = pi + diff
            
            for dir in range(idim):
                vec = comp[dir:dir+1,:]
                mat = np.matmul(vec.T,vec)
                diff = diff - np.matmul(mat,diff)
            Xnew[i,:] = pi + diff/ratio
        if verbose:
            print("Example variations: " + str(vars))
        X[:,:] = Xnew[:,:]
    return X
    
# local eccentricity used in (Mike and Perea 2019). 
def KNN_alt_ecc(X,k=10):
    (numpts,dim) = X.shape
    ecc = np.zeros(numpts)
    model = KNN(k+1,metric='euclidean')
    model.fit(X)
    subsize=5000
    for portion in range(int((X.shape[0]-1)/subsize)+1):
        submin = subsize*portion
        submax = min(numpts,subsize*(portion+1))
        Xsub = X[submin:submax,:]
        (dists,nbhds) = model.kneighbors(Xsub)
        for index in range(submin,submax):
            point = Xsub[index-submin,:]
            nbhd = X[nbhds[index-submin][1:],:]
            mean = np.mean(dists[index-submin][1:])
            cent = np.mean(nbhd,axis=0)
            offs = np.linalg.norm(cent-point)
            ecc[index] = offs/mean
    return ecc

# Simple cluster utility for use in e.g. mapper-ish stuff
def cluster(X,links='single',ratiocut = 0.7, maxcut = 10):
    Dmat = PWD(X)
    Dmat = (Dmat + Dmat.T) / 2
    Dvec = Dmat_Convert(Dmat, force='tovector')
    clusting = hierarchy.linkage(Dvec, method=links)
    cvec = clusting[::-1,2]
    rvec = np.array([cvec[x]/cvec[x-1] for x in range(1,len(cvec))])
    rvec[np.where(np.isnan(rvec))[0]] = 1
    crooks = np.where(rvec < ratiocut)[0]
    crooks = crooks[np.where(crooks <= (maxcut+2))]
    if len(crooks) == 0:
        numclusts = 1
    else:
        numclusts = np.max(crooks) + 2
    clab = hierarchy.cut_tree(clusting, n_clusters=numclusts)[:, 0]
    return clab


### GRAPH PLOTS API
def graph_pos(G, layout='spring', dim=2, weight='weight', pos=None,
              output='dict'):
    # prep numpy output:
    nn = len(G)
    X = np.zeros([nn, dim])
    center = np.zeros(dim)

    # prep starting positions from Graph attributes or given ndarray.
    if not pos == None:
        if type(pos) == str:
            pos = nx.get_node_attributes(G, 'pos')
        elif type(pos) == np.ndarray:
            if (not pos.shape[0] == nn) or (pos.shape[1] < dim):
                print('inappropriately shaped initial positions.')
                pos = None
            else:
                pos2 = {}
                for (i, node) in enumerate(G):
                    pos2[node] = pos[i, 0:dim]
                pos = pos2

    # choose layout type: possibly add force atlas from internets?
    if layout == 'spring':  # Fruchterman Reingold spring weights.
        pos = NDL.spring_layout(G, dim=dim, pos=pos, weight=weight, center=center)
    elif layout == 'spectral':  # laplacian EVs
        pos = NDL.spectral_layout(G, dim=dim, weight=weight, center=center)
    elif layout == 'kawai':  # path lengths doesn't seem to work?
        pos = nx.kamada_kawai_layout(G, dim=dim, pos=pos, weight=weight, center=center)

    if not output == 'dict':
        for (i, node) in enumerate(G):
            X[i, :] = pos[node]
        return X
    else:
        return pos


# graph_plot designed for nerve learning outputs. Lots of defaults.
def graph_plot_quick(G, vec=None, layout='spring', dim=2, TH=0, ax=None):
    nn = len(G)
    if type(vec) == type(None):
        vec = np.arange(nn)
    X = graph_pos(G, layout=layout, dim=dim, pos='pos', output='nparray')
    colors = colorprep(vec, shift=0.1, symm=True)
    ax = graph_plot(G, X, ax=ax, Ncolor=colors, Nsize='size', Ethresh=(TH, 'weight'))
    return ax


# graph plot with color and size.
def graph_plot_CS(G, colorattr='time', sizeattr='size', layout='spring',
                  dim=3, pow=0, vscale=10, maxiters=100):
    if type(colorattr) == type(None):
        cvec = np.array([1 for i in G.nodes()])
    else:
        c = nx.get_node_attributes(G, colorattr)
        cvec = np.array([c[i] for i in G.nodes()])

    s = nx.get_node_attributes(G, sizeattr)
    if layout == 'spring':
        print('computing layout.')
        pos = nx.drawing.layout.spring_layout(G, dim=dim, k=1 / len(cvec),
                                              iterations=maxiters, weight='weight')
    print('plotting graph.')
    svec = np.array([s[i] ** pow for i in G.nodes()])
    svec = svec / np.mean(svec) * vscale
    Ncolor = colorprep(cvec)
    X = np.array([pos[i] for i in G.nodes()])
    Es = [[pos[edge[0]], pos[edge[1]]] for edge in G.edges()]

    if dim == 3:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.scatter(xs=X[:, 0], ys=X[:, 1], zs=X[:, 2], s=svec, c=Ncolor)
        ax.add_collection3d(Line3DCollection(Es, linewidths=1))

    if dim == 2:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        lc = collections.LineCollection(Es, linewidths=1)
        ax.scatter(X[:, 0], X[:, 1], s=svec, c=Ncolor)
        ax.add_collection(lc)
    return


# semi-general NX graph_plot API: assume all nodes are included. SO MANY OPTIONS
def graph_plot(G, X, ax=None,
               Ncolor='r', Nsize='even', nodezoom=1, Nshape='o',
               Ecolor='k', Esize='even', edgezoom=1, Ethresh=(0, 'weight')):
    # number of nodes, dimension of ambient space
    (nn, dim) = X.shape

    # obtain visually-balanced node sizes:
    if type(Nsize) == str:
        if Nsize == 'even':
            Nsize = 500 / np.sqrt(nn)
        else:  # use the string as an attribute name
            Nwts = nx.get_node_attributes(G, Nsize)
            Nsize = np.ones(nn)
            for (i, node) in enumerate(G):
                Nsize[i] = np.sqrt(Nwts[node])
            total = np.sum(Nsize)
            Nsize = Nsize * 10000 / total
        Nsize = Nsize * nodezoom

    # obtain reduced edgelist: (is there a way to make this sorted?)
    if Ethresh[0] > 0:
        Evals = nx.get_edge_attributes(G, Ethresh[1])
        Elist = []
        for edge in G.edges():
            if Evals[edge] > Ethresh[0]:
                Elist.append(edge)
    else:
        Elist = G.edges()

    ne = len(Elist)
    print('There are ' + str(nn) + ' Nodes in the graph.')
    print('There are ' + str(ne) + ' Edges in the graph.')

    if type(Esize) == str:
        if Esize == 'even':
            Esize = 10 / np.sqrt(ne)

    if dim == 3:
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

        Es = [[X[edge[0], :], X[edge[1], :]] for edge in Elist]
        ax.scatter(xs=X[:, 0], ys=X[:, 1], zs=X[:, 2], s=Nsize, c=Ncolor, marker=Nshape)
        ax.add_collection3d(Line3DCollection(Es, colors=Ecolor, linewidths=Esize))
    elif dim == 2:
        if ax is None:
            fig, ax = plt.subplots()

        Es = [[X[edge[0], :], X[edge[1], :]] for edge in Elist]
        ax.scatter(X[:, 0], X[:, 1], s=Nsize, c=Ncolor, marker=Nshape)
        ax.add_collection(collections.LineCollection(Es, colors=Ecolor, linewidths=Esize))
        # networkX fails...
    elif dim == 2:
        pos = {}
        for i in range(nn):
            pos[i] = X[i, :]
        nx.draw_networkx(G, pos=pos, with_labels=labels, ax=ax,
                         alpha=alpha, font_size=font_size,
                         node_size=Nsize, node_color=Ncolor, node_shape=Nshape,
                         edgelist=Elist, width=Esize, edge_color=Ecolor, style=Eshape)
    else:
        print('Show graph only in dimension 2 or 3')

    return ax