## script for comparing embeddings created by::
# TALLEM (Topological Assembly of Locally Linear Models)
# Isomap    (DR.iso)
# DiffMaps  (DR.dmap)
# UMAP      (DR.umap)
# t-SNE     (DR.tsne)

# Typical stuff:
from tqdm import tqdm
import random
import numpy as np
import time
import os.path
import copy
import sys

# For plotting and showing stuff:
import matplotlib.pyplot as plt

# Toolkits with personal API:
import Data_Gen as DG
import Data_Rep as DR
import Class_Map as CM
import Fiber_Bundles as FB

# System specific file separator.
sep = os.sep

# Method used to determine the base mapping. If you don't have Ripser, use 'given'.
# 'cohomplus' uses persistent homology on nbhd geodesic distances and is used to
# create the images and values in (Mike and Perea 2019). 
baseType = 'cohomplus'
#baseType = 'cohom'
#baseType = 'linear'
#baseType = 'given'

baseP = {}
baseP['baseFinder'] = baseType
localP = {}

# simple presentations for the cylinder and mobius strip.
def cyl(As,Hs):
    return np.array([[np.cos(a),np.sin(a),h] for (a,h) in zip(As,Hs)])
def mob(As,Hs):
    return np.array([[(1+h*np.cos(a/2))*np.cos(a),
    (1+h*np.cos(a/2))*np.sin(a),h*np.sin(a/2)]
        for (a,h) in zip(As,Hs)])
def frm(As,Hs,frame):
    return np.array([[(1+h*f[0])*np.cos(a),(1+h*f[0])*np.sin(a),h*f[1]]
        for (a,h,f) in zip(As,Hs,frame)])

# methods for comparison in common dictionary:
mlist = ['iso','dmap','umap','tsne']
mname = ['Isomap','Diffusion Map',
    'Uniform Manifold Approximation and Projection',
    'T-Distributed Stochastic Neighbor Embedding']
(methods,methods['iso'],methods['dmap']) = ({},DR.isomap,DR.dmap)
(methods['umap'],methods['tsne']) = (DR.umap,DR.tsne)
                    
# SHARED DATASET PARAMETERS
numpts = 2000   # number of data points
stage = 'final'
dataType = sys.argv[1]
if dataType == 'PLBC':
    RI = 0.4
    localP['numcov']=64  
elif dataType == 'TLB1': 
    RI = 0.4
elif dataType == 'CCYL': 
    RI = 0.2
elif dataType == 'MOBC': 
    numpts = 10000
    RI = 0.4
    localP['numcov']=32

# prepare parameters for TALLEM
baseP['baseType'] = 'T1'
baseP['basedim'] = 1
if dataType == 'PLBC': # Trivial line bundle over circle (cylinder)
    (pts,dim,params,ftype,dtype) = DG.Patches_Cyl(numpts,BW=RI,aspect=5)
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
if dataType == 'CCYL': # Cylinder Coil
    (pts,dim,params,ftype,dtype) = DG.Coil_Cyl(numpts,rad_inn=RI,spins=0.5)
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
if dataType == 'TLB1': # Tautological Line bundle over RP1 (mobius band)
    (pts,dim,params,ftype,dtype) = DG.Patches_Mobius(numpts,BW=RI,aspect=5)
    baseP['baseCoords'] = np.array([[np.cos(2*a),np.sin(2*a)] for a in params[:,0]])
if dataType == 'MOBC': # mobius coil
    (pts,dim,params,ftype,dtype) = DG.mobius_coil(numpts,rad_inn = RI,spins=1.5)
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
color1 = DR.colorprep(params[:,0],symm=False)
color2 = DR.colorprep(params[:,1],symm=False)
Cvec = params[:,1]
    
### ---------- First do TALLEM coordinates: ---------- ###
FBI = FB.coords(pts,baseParams=baseP,localParams=localP,stage=stage,
    MV = 0,Cvec=Cvec)

print("Showing TALLEM correlogram")
fullparam = np.concatenate([params[:,:1],Cvec.reshape([len(Cvec),1])],axis=1)
SCCs = FBI['altout']
DR.multi_scatter(SCCs,fullparam,colors=color2)
plt.show()

print("Showing output of TALLEM via assembly frame (w/ rotations)")
# general application of TALLEM; the orient version is more appropriate
# for the cylinder examples, as we can take a trivial assembly frame. 
for offset in range(4):
    As = 2*np.pi*(FBI['altCoords'])
    Hs = FBI['altout'][:,1]
    angle = offset*np.pi/4
    (ca,sa) = (np.cos(angle),np.sin(angle))
    mat = np.array([[ca,sa],[-sa,ca]])
    X = frm(As,Hs,np.matmul(FBI['cframe'],mat))
    DR.scatter3(X,colors=color2)
    plt.axis('off')
    plt.show()

print("Showing output of TALLEM via orient")
# This is usually cleaner, but not as general.

Xt = FBI['orient']
if Xt == -1:
    param = mob
else:
    param = cyl
X = param(As,Hs)
DR.scatter3(X,colors=color2)
plt.axis('off')
plt.show()

# Comparison to other methods.
for (i,mtype) in enumerate(mlist):
    print("Showing output of "+mname[i])
    X = methods[mtype](pts,3)
    DR.scatter3(X,colors=color2)
    plt.axis('off')
    plt.show()

sys.exit()

# (comparing local coords)
simps = FBI['simps2']
edges = []
for (v1,v2) in simps[1]:
    if v1 == 0 or v2 == 0:
        edges.append([v1,v2])
