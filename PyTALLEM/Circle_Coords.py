## Script for the Circular coords maps on the dataset from
# Goddard et al. (Primate Neuron response to visual stimula)
# The process is a little different due to the 2d (circular) fibers.

import numpy as np
import os
import sys
from tqdm import tqdm
from scipy.io import loadmat
from scipy.io import savemat
import Data_Gen as DG
import Data_Rep as DR
import Class_Map as CM

# Platform-Specific File Separator:
sep = os.sep

NL = 30
THR1 = 4
THR2 = 2.5

# For brevity and simplicity, we just use 1 of the datasets.
# In particular, this is the file Moving_grating_spike_counts_ma027_ip.mat
# labels[:,0] is angle, labels[:,1] is the spatial frequency (1/speed)
(data,labels,portion) = DG.load_neuro(part=1,sample=1)

Xpca = DR.pca(data,outdim=6)
base_coords = Xpca[:,0]

outs = {}
outs['data'] = Xpca
outs['isom'] = DR.isomap(Xpca,outdim=6)
outs['base'] = base_coords
outs['labs'] = labels

savemat('derived_data.mat',outs)

#(Ls,cover_rad,Dmat) = CM.MM_geodesic(base_coords,nn=10,NL=NL)
#(rad1,rad2) = (THR1*cover_rad,THR2*cover_rad)
#(cover1,assign1,dists1) = CM.landmark_cover(Xpca,Ls,rad1)
#(cover2,assign2,dists2) = CM.landmark_cover(Xpca,Ls,rad2)
# custom local coords (based on rad2)
#models = []
#for (index,landmark) in enumerate(Ls):
#    Xlocal = Xpca[cover1[index],:]
#    Ylocal = DR.pca(Xlocal,outdim=2)
#    for row in Ylocal:
#        row = row/np.linalg.norm(row)
#    models.append(Ylocal)
#simps = CM.point_nerve(cover,maxdim=2)
#omega = [[np.zeros([2,2]) for x in simps[0]] for y in simps[0]]
#for (v1,v2) in simps[1]:
