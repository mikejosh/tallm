# Typical stuff:
from tqdm import tqdm
import random
import numpy as np
import time
import os.path
import copy
import sys

# For plotting and showing stuff:
import matplotlib.pyplot as plt

# Toolkits with personal API:
import Data_Gen as DG
import Data_Rep as DR
import Class_Map as CM
import Fiber_Bundles as FB

# System specific file separator.
sep = os.sep

# Method used to determine the base mapping. If you don't have Ripser, use 'given'.
# 'cohomplus' uses persistent homology on nbhd geodesic distances and is used to
# create the images and values in (Mike and Perea 2019). 
baseType = 'cohomplus'
#baseType = 'cohom'
#baseType = 'linear'
#baseType = 'given'

baseP = {}
baseP['baseFinder'] = baseType
localP = {}

# dataType in ['PLBC','CCYL','TLB1','MOBC','TLB2','SBT2']
dataType = sys.argv[1]

# SHARED DATASET PARAMETERS
numpts = int(sys.argv[2])   # number of data points.
RI = float(sys.argv[3])     # (inner radius) fiber length for several datasets
if len(sys.argv) > 4:       # number of cover sets.
    localP['numcov'] = int(sys.argv[4])
if len(sys.argv) > 5:       # number of landmarks for base map.
    baseP['minNL'] = int(sys.argv[5])

if len(sys.argv) > 6:
    stage = sys.argv[6]
else:
    stage = 'final'
# stage in ['base','cover','local','align','final']
    
### ---------- Assign info for chosen dataset ---------- ###
if dataType == 'PLBC': # Trivial line bundle over circle (cylinder)
    (pts,dim,params,ftype,dtype) = DG.Patches_Cyl(numpts,BW=RI,aspect=5)
    color1 = DR.colorprep(params[:,0],symm=False)
    color2 = DR.colorprep(params[:,1],symm=False)
    baseP['baseType'] = 'T1'
    baseP['baseCoords'] = np.array([[np.cos(a+np.pi),np.sin(a+np.pi)] for a in params[:,0]])
    baseP['basedim'] = 1
    Cvec = params[:,1]
if dataType == 'CCYL': # Coil Cylinder
    (pts,dim,params,ftype,dtype) = DG.Coil_Cyl(numpts,rad_inn=RI,spins=0.5)
    color1 = DR.colorprep(params[:,0],symm=False)
    color2 = DR.colorprep(params[:,1],symm=False)
    baseP['baseType'] = 'T1'
    baseP['baseCoords'] = np.array([[np.cos(a),np.sin(a)] for a in params[:,0]])
    baseP['basedim'] = 1
    Cvec = params[:,1]
if dataType == 'TLB1': # Tautological Line bundle over RP1 (mobius band)
    (pts,dim,params,ftype,dtype) = DG.Patches_Mobius(numpts,BW=RI,aspect=5)
    color1 = DR.colorprep(params[:,0],symm=False)
    color2 = DR.colorprep(params[:,1],symm=False)
    baseP['baseType'] = 'T1'
    baseP['baseCoords'] = np.array([[np.cos(2*a+np.pi),np.sin(2*a+np.pi)] for a in params[:,0]])
    baseP['basedim'] = 1
    Cvec = params[:,1]
if dataType == 'MOBC': # mobius coil
    (pts,dim,params,ftype,dtype) = DG.mobius_coil(numpts,rad_inn = RI,spins=1.5,hole=hole)
    color1 = DR.colorprep(params[:,0],symm=False)
    color2 = DR.colorprep(params[:,1],symm=False)
    baseP['baseType'] = 'T1'
    baseP['baseCoords'] = np.array([[np.cos(a+np.pi),np.sin(a+np.pi)] for a in params[:,0]])
    baseP['basedim'] = 1
    Cvec = params[:,1]
if dataType == 'TLB2': # Tautological Line bundle over RP2
    (pts,dim,params,ftype,dtype) = DG.Patches_LBonRP2(numpts,BW=RI,aspect=3)
    color1 = DR.RP2_color(CM.stereo_proj(params[:,:3],np.array([1,0,0])))
    color2 = DR.monocolor(params[:,3],symm=False) # on [0,1] (fibers)
    baseP['baseType'] = 'RP2'
    baseP['baseCoords'] = params[:,:3]
    baseP['basedim'] = 2
    Cvec = params[:,3]
    
    # Defornmation Retraction is needed for this example.
    if baseType  == 'cohomplus':
        Xnew = FB.retract(pts,k=50,gnn=10,bnn=300,pnn=20,prop=0.98,idim=2,
            minpts=2500)
        (landmarks_X,cover_rad_X,Dmat) = CM.MM_geodesic(Xnew,nn=20,NL=100)
        cover_rad_X = 2.5*cover_rad_X
        RPJ = CM.projective_coords(Xnew,landmarks_X,cover_rad_X,
            verbose=True,distfcn='precomputed',Dmat=Dmat,fullcheck=True)
        (RPJCs,perps,distorts) = CM.principal_projection(RPJ,dims2save=5)
        baseP['baseFinder'] = 'given'
        baseP['baseCoords'] = RPJCs[-3]

if dataType == 'SBT2': # Sum bundle over 2-Torus
    (pts,dim,params,ftype,dtype) = DG.SB_torus(numpts,FS=2*RI,gen=[1,1])
    color1 = DR.T2_color(params[:,[0,1]])
    color2 = DR.colorprep(params[:,2],symm=False)
    baseP['baseType'] = 'T2'
    baseP['baseCoords'] = np.array([[np.cos(a+np.pi),np.sin(a+np.pi),
        np.cos(b+np.pi),np.sin(b+np.pi)]
        for (a,b) in zip(params[:,0],params[:,1])])
    baseP['basedim'] = 2
    Cvec = params[:,2]    
    
### ---------- Call FBcoords ---------- ###

MV = int(localP['numcov']/5) # constant for viewing fibers.
# FBI means "Fiber Bundle Information"
FBI = FB.coords(pts,baseParams=baseP,localParams=localP,stage=stage,
    MV = MV,Cvec=Cvec)

# Use given altCoords for comparison
bp2 = copy.deepcopy(baseP)
bp2['baseFinder']='given'
if dataType == 'TLB2': # (b/c of retract) also fixes fullparam.
    bp2['baseCoords'] = params[:,:3]
FBE = FB.coords(pts,baseParams=bp2,stage='base',verbose=False)

### ---------- View the results from different PoV ---------- ###

# If stage is too early, just show base coordinates.
if stage in ['base','cover','local']:
    DR.scatterV(FBI['altCoords'],colors=color1)
    plt.show()
    sys.exit()

# plot orienting spantree over the base coords.
if 'spantree' in FBI.keys():
    DR.edge_scatter(FBI['altCoords'][FBI['landmarks_B'],:],
        FBI['spantree'].edges(),color1 = 'r', color2 = 'b', ax = None)
    plt.show()

# obtain full information about generation params and learned coords.
fullparam = np.concatenate([FBE['altCoords'],
                            Cvec.reshape([len(Cvec),1])],axis=1)
TALLEM = FBI['altout']

# full dataset images:
print("Showing P-C multiscatter, colored by base param")
DR.multi_scatter(TALLEM,fullparam,colors=color1)
plt.show()
print("Showing P-C multiscatter, colored by fiber param")
DR.multi_scatter(TALLEM,fullparam,colors=color2)
plt.show()
print("Showing coords fibercyl colored by fiber param")
DR.scatterV(TALLEM,colors=color2)
plt.show()
print("Showing params fibercyl colored by fiber coord")
DR.scatterV(fullparam,colors=DR.colorprep(TALLEM[:,-1],symm=False))
plt.show()

### ---------- investigate fiber + metric correlation ---------- ###

# Start with distance correlation on the base space.
print("Distance Correlation for Base Params and Base Coords:")
distbase = FBI['altMetric']
ss = np.random.choice(numpts,500,replace=False)
cc = TALLEM[ss,:-1]
pp = fullparam[ss,:-1]
basecorr = CM.dist_cor(cc,pp,dC=distbase,dP=distbase)
print("Base distance correlation: " + str(basecorr))

print("Local linear correlation for Fiber Param and Fiber Coords:")
(corrs,slopes,inters) = CM.local_fiber_corr(TALLEM,fullparam,FBI['cover2'],
    FBI['orient'],distfcn=distbase)

# align slopes/inters positively:
for (i,slope) in enumerate(slopes):
    if slope < 0:
        slopes[i] = -slopes[i]
        inters[i] = -inters[i]

MAC = np.mean(np.abs(corrs))
VAC = np.sqrt(np.var(np.abs(corrs)))
print("mean absolute correlation = " + str(MAC) + " +/- " + str(VAC))
PCoffset = np.sum(inters**2)/(len(inters)-1)
print("Param-Coord offset = 0 +/- " + str(PCoffset))
PCratio = np.mean(slopes) # (len(Cfibers) / len(Pfibers))
PCRvars = np.sqrt(np.var(slopes))
print("Mean Param-Coord ratio = " + str(PCratio) + " +/- " + str(PCRvars))

# Warning: Fiber lengths may not be isotropically related to base lengths.
# *VERY* time consuming! esp if we are using lots of points in RP2 xx [-a,a]
# sampling may be necessary to estimate dcor swiftly
distfibe = FBI['fibMetric']
(SS,rad,EDmat) = CM.MM_geodesic(pts,nn=10,NL=200)
ee = EDmat[:,SS][SS,:]

# smaller ratios are likely for shorter fibers (expect 4:1 for most 1D)
ratios = [2**(pow/2) for pow in list(range(-9,1))] # 4sqrt(2) (32:1 max:min)
PCcorr = np.zeros(len(ratios))
ECcorr = np.zeros(len(ratios))
for (j,jj) in tqdm(enumerate(ratios)):
    cc = TALLEM[SS,:]
    pp = fullparam[SS,:]
    cc[:,-1] = (PCratio*jj)*cc[:,-1]
    pp[:,-1] = jj*pp[:,-1]
    PCcorr[j] = CM.dist_cor(cc,pp,dC=distfibe,dP=distfibe)
    ECcorr[j] = CM.dist_cor(cc,ee,dC=distfibe,dP="given")

np.set_printoptions(precision = 3)
print(np.array([ratios,PCcorr,ECcorr]))
np.set_printoptions(precision = 8)

if dataType == 'TLB2': # view base coords in new colors, align to gray.
    newcolor = DR.RP2_color(fullparam[:,:2])
    Sks = FBI['basecoords']
    # "grayest point"
    index = np.argmin(np.linalg.norm(fullparam[:,:2],axis=1))
    vec = np.array([Sks[index]])
    proj = np.matmul(vec.T,vec)
    mat = np.linalg.eigh(proj)[1]
    Sks = np.matmul(Sks,mat)
    for i in range(numpts):
        if Sks[i,2] < 0:
            Sks[i,:] = - Sks[i,:]
    SPs = CM.stereo_proj(Sks,np.array([0,0,1]))
    DR.scatter2(SPs,colors=newcolor)

sys.exit()

# Compare (non-smoothed) local coordinates with and without offsets. 
simps = FBI['simps2']
edges = []
for (v1,v2) in simps[1]:
    if v1 == 0 or v2 == 0:
        edges.append([v1,v2])

CM.fiber_model_compare(pts,FBI['models'],FBI['cover1'],FBI['bary2'],
    FBI['offsets'],FBI['cocycle'],edges=edges,modeltype=localP['localType'])

# (1*,7,12) --- (6*,8*,11)
