### Top-level functions for fiber-bundle based coords.
### takes data and parameters, then unfolds full embedding information.

# Necessary stuff:
from tqdm import tqdm
import random
import numpy as np
import time
import os.path
import copy
import sys

# Standard Packages:
import matplotlib.pyplot as plt
import scipy

# Attached Toolkits:
import Class_Map as CM
import Data_Rep as DR
    
# Returns a dictionary to account for variable amounts of output information.
# stage in ['base','cover','local','align','final'] for intermediate analysis.

def coords(pts,baseParams={},localParams={},useoffs = 1,
        verbose=True,stage = 'final',MV = 0,Cvec = None):
    # pts is the dataset. 
    # stage determines how far into TALLEM pipeline to go.

    # FEEDBACK PARAMETERS:
    # verbose determines level of feedback while running.
    # MV gives views of fiber coords. MV = 0 shows none.
    # Cvec gives local parameters for coloring fiber coords plots.
    
    # TALLEM PARAMETERS:
    # Base space params (defaults)
    baseP = {'baseType':'RP2',        #  Base space B: RPk, Tk, circle, linear 
            'baseFinder':'cohom',     #  base coords method: cohom, given, iso
            'MLNN':20,                # #neighbors for manifold learning.
            'baseCoords':[],          #  [N x D] matrix
            'basedim':2,              #  dimension of B. 
            'THR':4,                  # cover ratio for persistent cohom.
            'minNL':200,              # #landmarks for persistent cohom.
            'metric':CM.eucl_dist}    # metric for original data.
            
    #cover and local params (defaults)
    localP = {'localType':'iso',      # local coords method: pca, iso, lle.
            'MLNN':10,                # #neighbors for manifold learning.
            'coverType':'IFP',        # Landmarks (Iterative Furthest Point)
            'localdim':1,             #*dimension of local coords.
            'THR1':2.5,               # larger cover ratio
            'THR2':1.5,               # smaller cover ratio
            'numcov':100}             #*number of landmarks/coversets/fibers
    
    # alternative values to the defaults.
    for key in baseParams:
        baseP[key] = baseParams[key]
    if baseP['basedim'] == 1:
        localP['THR1'] = 4
        localP['THR2'] = 2.5
        localP['numcov'] = 16
    for key in localParams:
        localP[key] = localParams[key]
    
    # Use baseParams and LocalParams to override defaults in baseP and localP.
    
    
    output = {}
### ---------- Determine BASE coords: ---------- ###
    if verbose:
        print("Finding Base Coordinates")

    if baseP['baseFinder'] == 'given':
        baseCoords = baseP['baseCoords']
    elif baseP['baseFinder'] == 'cohom': 
        (landmarks_X,cover_rad_X) = CM.max_min(pts,
            min_landmarks=baseP['minNL'],distfcn = baseP['metric'])
        print("Original cover radius is " + str(cover_rad_X))
        cover_rad_X = baseP['THR']*cover_rad_X
        print("Big cover radius is " + str(cover_rad_X))
        
        if baseP['baseType'] == 'circle':
            (baseCoords,RmZs) = CM.circular_coords(pts,landmarks_X,cover_rad_X,
                wgts = "dist",verbose = True,distfcn = baseP['metric'])
        if 'RP' in baseP['baseType']:
            outdim = baseP['basedim']
            PJCs = CM.projective_coords(pts,landmarks_X,cover_rad_X,maxfeatures=2,
                verbose=True,distfcn = baseP['metric'])
            (PPJCs,perps,distorts) = CM.principal_projection(PJCs,
                verbose=10,dims2save=outdim+3)
            baseCoords = PPJCs[-(outdim+1)]
        if 'T' in baseP['baseType']:
            outdim = baseP['basedim']
            (baseCoords,flatCoords) = CM.torus_coords(pts,landmarks_X,cover_rad_X,
                outdim=outdim,distfcn = baseP['metric'])
    elif baseP['baseFinder'] == 'cohomplus':
        (landmarks_X,cover_rad_X,Dmat) = CM.MM_geodesic(pts,nn=10,
            NL=baseP['minNL'],distfcn=baseP['metric'])
        print("Original cover radius is " + str(cover_rad_X))
        cover_rad_X = baseP['THR']*cover_rad_X
        print("Big cover radius is " + str(cover_rad_X))

        if baseP['baseType'] == 'circle':
            (baseCoords,RmZs) = CM.circular_coords(pts,landmarks_X,cover_rad_X,
                wgts="dist",verbose=True,distfcn='precomputed',Dmat=Dmat)
        if 'RP' in baseP['baseType']:
            outdim = baseP['basedim']
            PJCs = CM.projective_coords(pts,landmarks_X,cover_rad_X,maxfeatures=2,
                verbose=True,distfcn='precomputed',Dmat=Dmat)
            (PPJCs,perps,distorts) = CM.principal_projection(PJCs,
                verbose=10,dims2save=outdim+3)
            baseCoords = PPJCs[-(outdim+1)]
        if 'T' in baseP['baseType']:
            outdim = baseP['basedim']
            (baseCoords,flatCoords) = CM.torus_coords(pts,landmarks_X,cover_rad_X,
                outdim=outdim,distfcn='precomputed',Dmat=Dmat)        
    elif 'iso' in baseP['baseFinder'] or 'lle' in baseP['baseFinder']:
        MLtype = baseP['baseFinder']
        nn = baseP['MLNN']
        if baseP['baseType'] == 'linear':
            ML = CM.MLM[MLtype](n_neighbors=nn,n_components=1)
            baseCoords = ML.fit_transform(pts)
        elif baseP['baseType'] in ['circle','T1']: # seems risky for a torus.
            ML = CM.MLM[MLtype](n_neighbors=nn,n_components=2)
            X = ML.fit_transform(pts)
            angles = np.arctan2(X[:,0],X[:,1])
            baseCoords = np.array([[np.cos(a),np.sin(a)] for a in angles])
            RmZs = angles/(2*np.pi)
    
    # assign metrics and flat coords (in B' in the paper)
    if baseP['baseType'] == 'circle': # this is obsolete
        baseMetric = CM.Sk_dist
        altCoords = CM.Flatten_Torus(baseCoords)
        altMetric = CM.peri_dist
    if 'RP' in baseP['baseType']:
        baseMetric = CM.proj_dist
        perp = np.zeros(baseCoords.shape[1])
        perp[-1] = 1 # flatten the last coord
        altCoords = CM.stereo_proj(baseCoords,perp)
        altMetric = CM.stereo_dist
    if baseP['baseType'] == 'linear':
        baseMetric = CM.eucl_dist
        altCoords = baseCoords
        altMetric = CM.eucl_dist
        fibMetric = CM.eucl_dist
    if 'T' in baseP['baseType']:
        baseMetric = CM.Tk_dist
        altCoords = CM.Flatten_Torus(baseCoords)
        if len(altCoords.shape) == 1: # enforce 2D matrix coords.
            altCoords = np.array([[x] for x in altCoords])
        altMetric = CM.tor_dist
        
    output['baseCoords'] = baseCoords
    output['altCoords'] = altCoords
    output['altMetric'] = altMetric
        
    if stage == 'base':
        return output
    
### ---------- Determine Cover and Barycentric Coords ---------- ###
    if verbose:
        print("Finding Cover of base coordinates")
    
    if localP['coverType']=='even' and baseP['baseType']=='circle':
        (info1,info2) = CM.circle_cover(localP['numcov'],
            localP['THR1'],(2*np.pi*altCoords),THRR = localP['THR2'])
        (cover1,assign1,dists1,rad_B1) = info1
        (cover2,assign2,dists2,rad_B2) = info2
    else: # default to IFP
        print(baseCoords.shape)
        (landmarks_B,cover_rad_B) = CM.max_min(baseCoords,
            min_landmarks=localP['numcov'],distfcn=baseMetric)
        rad_B1 = localP['THR1']*cover_rad_B
        rad_B2 = localP['THR2']*cover_rad_B
        (cover1,assign1,dists1) = CM.landmark_cover(baseCoords,landmarks_B,rad_B1,
            distfcn=baseMetric)
        (cover2,assign2,dists2) = CM.landmark_cover(baseCoords,landmarks_B,rad_B2,
            distfcn=baseMetric)
        output['landmarks_B'] = landmarks_B

    
    # barycentric coords
    bary1 = CM.bary_coords(assign1,dists1,rad_B1)
    bary2 = CM.bary_coords(assign2,dists2,rad_B2)
    
    output['cover1'] = cover1
    output['cover2'] = cover2
    output['assign1'] = assign1
    output['assign2'] = assign2
    output['dists1'] = dists1
    output['dists2'] = dists2
    output['rad_B1'] = rad_B1
    output['rad_B2'] = rad_B2
    output['bary1'] = bary1
    output['bary2'] = bary2
    output['landmarks_B'] = landmarks_B
    
    if stage == 'cover':
        return output

### ---------- Determine Local Models ---------- ###
    if verbose:
        print("Finding Local Models")
    if localP['localType'] == 'kpca':
        models = CM.kpca_models(pts,cover1,localP['localdim'],BW=BW,
            verbose=MV,Cvec=Cvec)
        if MV:
            (models,topvars,remvars) = models
    elif localP['localType'] in ['iso','lle','lta']:
        models = CM.mani_models(pts,cover1,localP['localdim'],
            nn=localP['MLNN'],MLtype=localP['localType'],
            verbose=MV,Cvec=Cvec)
        if MV:
            (models,vars) = models
    elif localP['localType'] == 'pca':
        models = CM.pca_models(pts,cover1,localP['localdim'],
            verbose=MV,Cvec=Cvec)
        if MV:
            (models,topvars,remvars) = models
        
    output['models'] = models
    
    if stage == 'local':
        return output
        
### ---------- Aligning Local Models ---------- ###
# current version assumes localdim = 1.
    if verbose:
        print("Aligning local models")
    
    if localP['localdim'] == 1:
        # Align the coords via cocycle
        if localP['localType'] == 'pca':
            (cocycle,shifts,orths,RMSE) = CM.pca_align(pts,cover1,models)
        else:
            (cocycle,shifts,orths,RMSE) = CM.line_align(pts,cover1,models)
        output['orths'] = orths
        
        # quick cocycle check (full and restrained nerves)
        simps1 = CM.point_nerve(cover1)
        simps2 = CM.point_nerve(cover2)
        output['simps1'] = simps1
        output['simps2'] = simps2
        if verbose:
            print('Problem Triangles in the (smaller) nerve complex:')
            print(CM.cocycle_check_Z2(simps2[2],cocycle))
        
        # readjust cocycle for AGA-TALLEM.
        if baseP['baseType'] == 'circle':
            (orient,flip_index) = CM.readjust_CL_align(
                cocycle,models,modeltype=localP['localType'])
            output['flip_index'] = flip_index
            output['orient'] = orient
        elif ('RP' in baseP['baseType'] 
            or 'T' in baseP['baseType']
            or baseP['baseType'] == 'linear'):
            (root,spantree,graph,crosses) = CM.sim_MST_graph(altCoords,
                landmarks_B,cover2,distfcn = altMetric)
            output['crosses'] = crosses
            output['spantree'] = spantree
            orient = CM.tree_line_align(altCoords,landmarks_B,cocycle,
                models,root,spantree,crosses,modeltype=localP['localType'])
            output['orient'] = orient
        
        # Improve Alignment via an offset for each local model.
        offs = CM.model_align_1d(cover2,cocycle,shifts)
        output['offsets'] = offs
        output['cocycle'] = cocycle

        
        if 'RP' in baseP['baseType']:
            def fibMetric(pt1,pt2):
                return CM.SPB_dist(pt1,pt2,orient=orient)
            output['fibMetric'] = fibMetric
        if 'T' in baseP['baseType']:
            def fibMetric(pt1,pt2):
                return CM.TkB_dist(pt1,pt2,orient=orient)
            output['fibMetric'] = fibMetric
        if baseP['baseType'] == 'linear':
            output['fibMetric'] = CM.eucl_dist
        
        if stage == 'align':
            return output
    
### ---------- Classifying Map/Frame ---------- ###
    if verbose:
        print("Extracting assembly frame and AGA coords.")
    
    if localP['localdim'] == 1:
        PJCs = CM.projective_param(bary2,cocycle,assign2)
        (PPJCs,perps,distorts) = CM.principal_projection(PJCs)
        frames = PPJCs[-2]  # Might need higher dim in general.
        output['cframe'] = frames
        
        # Specialized functions for viewing AGA-TALLEM in minimal dimensions.
        if baseP['baseType'] == 'circle':
            ATs = CM.CL_square_present(pts,altCoords,models,assign2,
                cover1,bary2,cocycle,flip_index,orient=orient,
                modeltype=localP['localType'],offsets=offs)
            
        if 'RP' in baseP['baseType']:
            ATs = CM.RP2L_cyl_present(pts,altCoords,models,assign2,
                cover1,bary2,cocycle,landmarks_B,orient=orient,
                modeltype=localP['localType'],offsets=offs)
            
        if 'T' in baseP['baseType']:
            ATs = CM.TL_cube_present(pts,altCoords,models,assign2,
                cover1,bary2,cocycle,landmarks_B,orient=orient,
                modeltype=localP['localType'],offsets=offs)
        if baseP['baseType'] == 'linear':
            ATs = CM.Flat_present(pts,altCoords,models,assign2,
                cover1,bary2,landmarks_B,localP['localType'],
                offsets = offs)
        
        # General TALLEM embedding in B x R^k. 
        GTs = CM.gen_present(pts,baseCoords,frames,models,assign2,cover1,
            bary2,cocycle,localP['localType'],offsets = offs)
        
        output['genout'] = GTs
        output['altout'] = ATs
        
    return output

# Big function for flattening a dataset as preprocessing for persistent homology.
# essentially deformation retracts a line bundle onto its base space. 
# skewness of eccentricity is used as a stopping criterion,
# as it roughly measures interior size.
def retract(pts,k=50,gnn=10,bnn=300,pnn=20,prop=0.98,idim=2,
    minpts=3000,minskew=0,verbose=False):
    """
    Parameters:
    k       | number of neighbors to calculate local eccentricity
    gnn     | number of neighbors to estimate geodesic distance (verb only)
    bnn     | number of neighbors to flatten each section
    pnn     | number of neighbors in iterative projection
    prop    | proportion of top eccentric points skimmed per iteration
    idim    | desired intrinsic dimension of the base manifold
    minpts  | minimum number of points in each section
    minskew | minimum skewness used in stopping criterion
    verbose | level of feedback given by the function. Some is always supplied
                amt of feedback: False < True < 100 < 10 < 1
    NOTE: In practice, the NNs used STRONGLY depends on idim
    rough guidelines: (uneven density/fiber length may disturb)
    idim=1 => k=10-20, gnn=5-15,  bnn=30-60,   pnn=4-5
    idim=2 => k=40-80, gnn=10-20, bnn=100-200, pnn=15-20
    """
    (numpts,dim) = pts.shape
    numsub = numpts
    max_iter = int(np.log(minpts/numpts)/np.log(prop))

    ss = np.array(range(numpts))
    X = pts[ss,:]
    peels = []
    skew = np.inf
    ### ---------- Stage I: Peel off Sections ---------- ###
    print("Begin by peeling off fiber edges for initial retraction")
    # the edge of the fibers is detected via "edge detection,"
    # in part. measuring deviation of kNN nbhd mean relative to kNN nbhd distance. 
    if verbose:
        for iter in range(max_iter):
            eccs = DR.KNN_alt_ecc(X,k=k)
            skewold = skew
            skew = scipy.stats.skew(eccs)
            if skew < minskew or (skew < minskew+0.2 and skew > skewold):
                break
            if (type(verbose) == type(True)) and (verbose == True):
                print("numsub = "+str(numsub)+", skew = "+str(skew))
            if type(verbose) == type(1) and iter%verbose == 0:
                DR.scatterV(fibercyl[ss,:],colors=DR.colorprep(eccs,symm=False))
                plt.show()
                kde = DR.simple_KDE(eccs,numvals=1000,BWratio=30)
                DR.scatterV(kde)
                plt.show()
            numsub = int(prop*numsub)
            # store forgotten points
            ssnew = ss[np.argsort(eccs)[:numsub]]
            peels.append(CM.setdiff(ss,ssnew))
            ss = ssnew
            X = pts[ss,:]
    else:
        for iter in tqdm(range(max_iter)):
            eccs = DR.KNN_alt_ecc(X,k=k)
            skewold = skew
            skew = scipy.stats.skew(eccs)
            if skew < minskew or (skew < minskew+0.2 and skew > skewold):
                break
            numsub = int(prop*numsub)
            # store forgotten points
            ssnew = ss[np.argsort(eccs)[:numsub]]
            peels.append(CM.setdiff(ss,ssnew))
            ss = ssnew
            X = pts[ss,:]

    print("Peeling ended after "+str(iter)+" iters with "+str(numsub)+" pts.")
        
    ## view peels:
    if verbose:
        print("Next viewing peeling index:")
        peelvec = np.zeros(numpts)
        for (i,peel) in enumerate(peels):
            peelvec[peel] = i
        peelvec[ss] = i+1
        DR.scatterV(fibercyl,colors=DR.colorprep(peelvec,symm=False))
        plt.show()

    # gather peels into set of "sections" with similar width to the remainder.
    thickpeels = [ss]
    thickness = len(ss)
    peelj = np.zeros(0,dtype='int64')
    for peel in reversed(peels):
        peelj = np.concatenate([peelj,peel],axis=0)
        if peelj.shape[0] > thickness:
            thickpeels.append(peelj[:])
            peelj = np.zeros(0,dtype='int64')
        
    ### ---------- Stage II: Flatten Sections ---------- ###
    # NOTE: special treatment for first noncore peel (clustering nbhds)
    # This is the most time-consuming step due to large NNs.
    Ys = []
    for (index,peel) in enumerate(thickpeels):
        print("Now flattening peel number " + str(index))
        X = pts[peel,:]
        for MIs in range(1): # possibly only 1?
            if verbose: # VIEW SOME COVERS
                (landmarks_X,cover_rad_X,Dmat) = CM.MM_geodesic(X,nn=gnn,
                    NL=baseP['minNL'],distfcn=baseP['metric'])
                print("Original cover radius is " + str(cover_rad_X))
                cover_rad_X = baseP['THR']*cover_rad_X
                print("Big cover radius is " + str(cover_rad_X))

                (cover,assign,dists) = CM.landmark_cover(Dmat,landmarks_X,cover_rad_X,
                    distfcn='precomputed')

                coverlens = np.array([len(x) for x in cover])
                print("min cover size = " + str(np.min(coverlens)))
                print("max cover size = " + str(np.max(coverlens)))
                CM.mani_models(X,cover,1,nn=gnn,MLtype='pca',rescale=False,
                    verbose=30,Cvec = preFibe[peel])
            if index == 1:
                doclust = True
            else:
                doclust = False
            X = DR.MBMS(X,nn=bnn,idim=idim,max_iters=1,ratio=1,QQ=0.05,
                doclust=doclust)
        Ys.append(X[:,:])

    # view distance to each Y layer:
    if verbose:
        for Y in Ys:
            model = DR.KNN(1)
            model.fit(Y)
            (dists,nbhds) = model.kneighbors(pts,return_distance = True)
            DR.scatterV(fibercyl,colors=DR.colorprep(dists,symm=False))
            plt.show()

    ### ---------- Stage III: Iterative Projection to Core Section ---------- ###
    # "unroll" subsets by layer projections (requires multiple 1-sided MBMS)
    print("Now Performing Iterative Projection Along Peels")
    Xnew = np.zeros([numpts,dim])
    peel = peelj
    X = pts[peel,:]
    print("Projecting leftover peel")
    for Y in reversed(Ys):
        X = DR.MBMS2(X,Y,nn=pnn,BWR=1,idim=idim,doclust=True) # only once?
    Xnew[peel,:] = X[:,:]
    for (index,peel) in enumerate(thickpeels):
        print("Projecting Peel number "+str(index))
        X = pts[peel,:]
        for Y in reversed(Ys[:index+1]): # repeated projections down.
            X = DR.MBMS2(X,Y,nn=pnn,BWR=1,idim=idim,doclust=True) # only once?
        Xnew[peel,:] = X[:,:]
    
    if verbose:
        return (Xnew,thickpeels+[peelj],peelvec)
    else:
        return Xnew


