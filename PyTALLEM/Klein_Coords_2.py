## Script for the Circular coords maps on the Van Hateren
# high-contrast natural images dataset, to be used to find
# Klein-bottle coordinates in MatTallem

import numpy as np
import os
import sys
from tqdm import tqdm
from scipy.io import loadmat
from scipy.io import savemat
import Data_Gen as DG
import Data_Rep as DR
import Class_Map as CM
import matplotlib.pyplot as plt

# Find the Dmatrix for a given aspect size:
def Dmatrix(aspect):
    dim = aspect**2
    Dmat = np.zeros([dim,dim])
    # Find the neighbors in the patch and sum differences
    # according to quadratic form sum_{i~j} (x_i-x_j)^2 = xDx
    for i in range(aspect):
        for ii in range(aspect):
            for dir in [(1,0),(0,1)]: #adjacent index
                j = i+dir[0]
                jj = ii+dir[1]

                if (0 <= j < aspect) and (0 <= jj < aspect):
                    #print('('+str(i)+','+str(ii)+')--('+str(j)+','+str(jj)+')')
                    islot = i*aspect+ii
                    jslot = j*aspect+jj
                    Dmat[islot,jslot] = Dmat[islot,jslot] - 1
                    Dmat[jslot,islot] = Dmat[jslot,islot] - 1
                    Dmat[islot,islot] = Dmat[islot,islot] + 1
                    Dmat[jslot,jslot] = Dmat[jslot,jslot] + 1
    return Dmat

def toDCT_G(X,aspect,verbose=False):
    Dmat = Dmatrix(aspect)
    (u,s,vh) = np.linalg.svd(Dmat)
    COB1 = np.matmul(u,np.diag(np.sqrt(np.abs(s))))
    COB2 = np.matmul(np.diag(np.sqrt(np.abs(1/s))),vh)
    Xnew = np.zeros(X.shape)
    for index in range(X.shape[0]):
        Xnew[index,:] = np.matmul(X[index,:],COB1)
    if verbose == 2:
        return (Xnew,COB1,COB2)
    elif verbose:
        return(Xnew,COB2)
    else:
        return Xnew

# Platform-Specific File Separator:
sep = os.sep

numpts = int(sys.argv[1])
NL = int(sys.argv[2])
NN = int(sys.argv[3])
THR = 1.5

# perhaps I should just make a large dataset to load for larger analysis.
(prepts,dim,labels,ltype,dtype)=DG.Patches_Klein(numpts)
pts = toDCT_G(prepts,3,verbose=False)
# preprocess?
(Xpca,vars,vecs,mean) = DR.pca(pts,outdim=dim,verbose=True)
print(vars)
DR.scatter3(Xpca)

(Ls,cover_rad,Dmat) = CM.MM_geodesic(Xpca,nn=40,NL=NL)
min_cover_rad = THR*cover_rad
print("Minimal cover radius = "+str(cover_rad))
(X,F) = CM.torus_coords(Xpca,Ls,min_cover_rad,outdim=1,TL = 1/2,TU=7/8)

DR.scatter3(Xpca,colors=DR.colorprep(F))
plt.show()

def view_subset(start,rad):
    subset = np.where(np.mod(F-start-rad,1)<2*rad)[0]
    Xsub = Xpca[subset,:]
    Fsub = F[subset]
    Xiso = DR.isomap(Xsub,outdim = 3,nn=30)
    DR.scatter3(Xiso,colors=DR.colorprep(Fsub))
    plt.show()

def circle_subset(start,rad,numL):
    subset = np.where(np.mod(F-start-rad,1)<2*rad)[0]
    Xsub = Xpca[subset,:]
    Fsub = F[subset]
    Xiso = DR.isomap(Xsub,outdim = 3,nn=30)
    (local_Ls, local_rad, local_Dmat) = CM.MM_geodesic(Xsub, nn=40, NL=numL)
    (localC, localF) = CM.torus_coords(Xsub,local_Ls,1.5*local_rad,outdim=1)
    DR.scatter3(Xiso,colors=DR.colorprep(Fsub))
    plt.show()
    DR.scatter3(Xiso,colors=DR.colorprep(localF))
    DR.scatter3(Xpca[subset,:],colors=DR.colorprep(localF))
    stuff = np.array([[x[0],x[1],y] for (x,y) in zip(localC,Fsub)])
    DR.scatter3(stuff,colors=DR.colorprep(localF))
    plt.show()

outs = {}
outs['data'] = Xpca
outs['base'] = X
outs['circ'] = F
outs['labs'] = labels
outs['raw'] = prepts
savemat('edges_gen_'+str(numpts)+'.mat', outs)
