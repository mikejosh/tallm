This package implements and analyzes the TALLEM (Topological Assembly of Locally Euclidean Models) dimensionality reduction procedure presented in the article of matching name,
JL Mike and JA Perea (2019) "TALLEM: Topological assembly of locally euclidean models" (preprint)
This distribution contains 2 separately implemented versions of TALLEM: one in matlab, and the other in python.

### ---------- matTALLEM ---------- ###
The matlab TALLEM implementation is designed for use without base mappings and includes the matlab implementation for PCAL (see the readme file in the folder for details on this method), which is redistributed here with a minor modification (a mollifying schedule for the objective function) under GNU general license. 

Besides the many intermediate functions used to implement TALLEM, 
the MatTALLEM folder includes 2 PCAL demos and a primary script, Examples_nobase.
The parameters for the script are listed at the top along with 4 datasets. The 'FACE' key loads a sample of the Brey Faces dataset shown in both [Roweis et al 2002] and [Brand 2003] (numpts = 1965 loads all of them), while the 'NIST' key loads a sample of the chosen digits from the MNIST dataset (numpts = 2000 is used in our paper). 
The script ships with parameters as used in our paper, though the curious user is encouraged to see the effects of changing parameters here.

MatTALLEM has 2 main components lacking in PyTALLEM: first, the dimensionality reduction procedure for d > 2;
and second, implementation of the final TALLEM mapping for non-cocycle Omega (the Procrustes alignments in O(d)).
Indeed, for d = 1, O(d) = {-1,1} so that a cocycle is a reasonable expectation. 

### ---------- pyTALLEM ---------- ###

The Python TALLEM implementation is designed for use with Ripser for persistent cohomology and in particular the designation of base mappings via projective or circular coordinates. 
This implementation includes the almost-global-alignment and deformation retraction methods for the examples used in our paper. 
The primary limitation here is that PyTALLEM only handles local dimension d = 1.

The following command line inputs should be used with the "Examples_base" script to generate our examples and images:

| Ex# | NAME | numpts |  RI  | #covs | #landmarks |
| --- | ---- | ------ | ---- | ----- | ---------- |
|  1  | TLB1 | 2000   | 0.4  |  16   |    200     |
|  2  | CCYL | 2000   | 0.2  |  16   |    200     |
|  3  | SBT2 | 2000   | 2    |  100  |    500     |
|  4  | TLB2 | 10000  | 0.5  |  100  |    100     |
|  5  | MOBC | 10000  | 0.4  |  32   |    200     |

These also generate the correlation values reported in the paper,
which are printed directly in the terminal. 

For the "Examples_compare" script, only use the dataset name for the command-line input (CCYL, TLB1, MOBC, and PLBC)

PyTALLEM file description:

Fiber_Bundles (as FB) contains the primary calls for TALLEM (FB.coords) and deformation retraction (FB.retract).
FB.coords returns a dictionary with a plethora of extra information, but the 'genout' and 'altcoords'/'altout' keys yield the TALLEM and AGA-TALLEM coordinates.

FB.coords is designed for implementation with general base space B, and therefore includes room for custom metrics on the dataset and base space, and also generates the appropriate metric on the AGA_TALLEM codomain.

Class_Map (as CM) contains many files which deal with the topological constructions in TALLEM, including implementations for circular and projective coordinates which are called by the FB.coords function.

Data_Rep contains many separate functions for plotting and reducing data which are not core to TALLEM, but are helpful for the analysis. 

Data_Gen contains functions to generate or load datasets.

### ---------- Requisite Packages ---------- ###
MatTALLEM:
Requires (a slightly modified) PCAL package, included here with attribution in its own readme (Bin Gao and Xin Liu).
We also use imgScatter package (Dimitris Floros) to create the image scatterplots for the Frey faces and MNIST eights.

PyTALLEM:
Calculation of cocycles for projective and circular coordinates rely upon PyRipser.
Moreover, umap (McInnes and Healy) and pydiffmap (Thiede, Banisch, Trstanova) packages are needed for outside comparisons;
other comparison methods are obtained through sklearn and scipy. 

### ---------- Bibliography ---------- ###

Roweis, S. T., Saul, L. K., & Hinton, G. E. (2002). Global coordination of local linear models. In Advances in neural information processing systems (pp. 889-896).

Brand, M. (2003). Charting a manifold. In Advances in neural information processing systems (pp. 985-992).

### ---------- GNU License ---------- ###

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/)