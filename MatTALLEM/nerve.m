function [simps] = nerve(cover)
%NERVE Finds the nerve of the given cover, up to triangles.

    NL = size(cover,1);
    NE = 0;
    NT = 0;
    
    for i = 1:NL
        for j = (i+1):NL
            Cij = intersect(cover{i},cover{j});
            if ~isempty(Cij)
                NE = NE+1;
                for k = (j+1):NL
                    Cijk = intersect(Cij,cover{k});
                    if ~isempty(Cijk)
                        NT = NT+1;
                    end
                end
            end
        end
    end
    
    simps = cell(3,1);
    simps{1} = (1:NL)';
    simps{2} = zeros(NE,2);
    simps{3} = zeros(NT,3);
    NE = 0;
    NT = 0;
    
    for i = 1:NL
        for j = (i+1):NL
            Cij = intersect(cover{i},cover{j});
            if ~isempty(Cij)
                NE = NE+1;
                simps{2}(NE,:) = [i,j];
                for k = (j+1):NL
                    Cijk = intersect(Cij,cover{k});
                    if ~isempty(Cijk)
                        NT = NT+1;
                        simps{3}(NT,:) = [i,j,k];
                    end
                end
            end
        end
    end
    
end

