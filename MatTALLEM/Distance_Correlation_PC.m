function [dCor] = Distance_Correlation_PC(X,Y)
%DISTANCE_CORRELATION_PC
    dCovXY = Distance_Covariance_PC(X,Y);
    dVarX = Distance_Covariance_PC(X,X);
    dVarY = Distance_Covariance_PC(Y,Y);
    
    dCor = dCovXY/(sqrt(dVarX*dVarY));
end

