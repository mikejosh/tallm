function [pts,dim,params] = gen_klein_edges(numpts,aspect,holesize,holedepth)
% GEN_KLEIN_EDGES generates a dataset of edges on the klein bottle which
% lie within a sphere.
    if holesize >= pi+1
        disp("hole size too large. Walk it back!")
        return
    end
    dim = aspect^2;
    pts = zeros(numpts,dim);
    params = zeros(numpts,3);
    % inbuilt klein-bottle distance to obtain regions of low density, based
    % on distance from 4 corner points. (ie, density = uniform - some
    % distance (use some nice ratio sampling off of uniform measure)
    % For now, assume the holes are gradual.
    
    % The four corners are represented by:
    % angle1 = pi/4 and 3pi/4
    % angle2 = pi/2 and 3pi/2
    nn = 1;
    while nn <= numpts
        angles = rand(2,1);
        angle1 = 2*pi*angles(1)-pi;
        angle2 = 2*pi*angles(2)-pi;
        d1 = KB_distance([angle1,angle2],[ 0,+pi/2]);
        d2 = KB_distance([angle1,angle2],[ 0,+pi/2]);
        d3 = KB_distance([angle1,angle2],[pi,-pi/2]);
        d4 = KB_distance([angle1,angle2],[pi,-pi/2]);
        prob = 1 - holedepth*(holesize-min([d1,d2,d3,d4]))/holesize;
        if rand(1) < prob
            params(nn,:) = [angle1,angle2,prob];
            nn = nn + 1;
        end
    end
    for nn = 1:numpts
        angle1 = params(nn,1);
        angle2 = params(nn,2);
        for ii = 1:aspect
            xval = -1 + (2*ii-1)/aspect;
            for jj = 1:aspect
                yval = -1 + (2*jj-1)/aspect;
                % Function is (alpha)*<x,v> + (beta)*[<x,v>^2 - 1/3]/4
                % to obtain similar levels of variation.
                slot = aspect*(ii-1) + jj;
                IP = xval*cos(angle1/2) + yval*sin(angle1/2);
                pts(nn,slot) = sin(angle2)*IP + 1.4*cos(angle2)*(IP^2-1/3);
            end
        end
        
    end
    return