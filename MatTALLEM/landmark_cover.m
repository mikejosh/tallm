function [cover,assign,dists] = landmark_cover(X,L,R)
%LANDMARK_COVER determines cover sets and assignments for datapoints.
%   Given points X, find point covers B(Li,R) intersect X.
    numpts = size(X,1);
    NL = length(L);
    coversizes = zeros(NL,1);
    assignsize = zeros(numpts,1);
    for i = 1:numpts
        for j = 1:NL
            dist = norm(X(i,:) - X(L(j),:));
            if dist < R
                coversizes(j) = coversizes(j) + 1;
                assignsize(i) = assignsize(i) + 1;
            end
        end
    end
    
    cover = cell(NL,1);
    assign = cell(numpts,1);
    dists = zeros(numpts,NL);
    for i = 1:numpts
        assign{i} = zeros(assignsize(i),1);
    end
    for j = 1:NL
        cover{j} = zeros(coversizes(j),1);
    end
    
    coversizes = zeros(NL,1);
    assignsize = zeros(numpts,1);
    for i = 1:numpts
        for j = 1:NL
            dist = norm(X(i,:) - X(L(j),:));
            if dist < R
                coversizes(j) = coversizes(j) + 1;
                assignsize(i) = assignsize(i) + 1;
                assign{i}(assignsize(i)) = j;
                cover{j}(coversizes(j)) = i;
                dists(i,j) = dist;
            end
        end
    end
    
    for j = 1:NL
        cover{j} = sort(cover{j});
    end
    
    mu = num2str(mean(coversizes));
    sd = num2str(std(coversizes));
    cm = num2str(min(coversizes));
    cM = num2str(max(coversizes));

    disp(['Cover sizes in [',cm,',',cM,'] mean ',mu,' +/- ',sd])
    
    
end

