function [index] = quickfind(vec, val)
%QUICKFIND performs binary search on (ordered!) vec for val.
    
    num = size(vec,1);
    left = 1;
    right = num;
    while left <= right
        mid = ceil((left+right)/2);
        if vec(mid) == val
            index = mid;
            return
        elseif vec(mid) > val
            right = mid-1;
        else
            left = mid+1;
        end
    end
    index = -1;
end

