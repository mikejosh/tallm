function [alpha,Omega,models] = spantree_alignment(pts,alpha,Omega,models,spantree,graph)
%SPANTREE_ALIGNMENT takes in models and a spantree and attempts to align
%the resulting models as well as it can.
%   The new models are inconsistent and must be smoothed over twice. 
%   Since Omega is not a precise cocycle, this process may be prone to 
%   error propagation.
    
    root = 1; % default option.
    % K = length(crosses) %% number of independent 1-cocycles in base.
    NV = length(models);
    numpts = size(pts,1);
    
    % Flip models according to cocycle alignment so that (nearly)
    % everything is oriented along the spantree (but not over the whole graph!!)
    allverts = (1:NV)';
    vert = root;
    
    % homemade stacks for graph exploration: "visited" and "margin" nodes.
    visited = zeros(NV,1);
    margin = zeros(NV,1);
    visited(1) = root;
    margin(1) = root;
    Nvisited = 1;
    Nmargin = 1;
    while Nvisited < NV % Go until we've visited every vertex.
        vi = margin(Nmargin);  % margin pop.
        Nmargin = Nmargin - 1; % margin pop.
        branches = find(spantree(vi,:)); % Grab adjacent verts.
        branches = setdiff(branches,visited); % but don't revisit verts.
        for vj = branches % adjust Omega along edges incident to vj.
            Oij = Omega{vi,vj};
            models{vj} = models{vj}*Oij; % Not backward.
            for vk = find(graph(vj,:)) % grab all adjacent verts
                Omega{vj,vk} = Omega{vj,vk}*Oij;  % vk = vi yields identity.
                Omega{vk,vj} = Oij'*Omega{vk,vj}; % vk = vi yields identity.
                alpha{vj,vk} = alpha{vj,vk}*Oij;  %
            end
            Nvisited = Nvisited + 1; % visited push (branches)
            visited(Nvisited) = vj;  % visited push (branches)
            Nmargin = Nmargin + 1; % margin push (branches)
            margin(Nmargin) = vj;  % margin push (branches)
        end
    end
    % The following requires a specific base model to characterize the
    % types of matrices.
    % As a blind alternative, we can simply cluster in R(d^2) or plot PCA. 
end

