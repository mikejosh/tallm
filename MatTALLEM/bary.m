function [barys] = bary(dists,cover,radius)
%BARY finds barycentric coordinates.
%   From a collection of dists and radius; dists may be sparse (to radius).
%   The landmark/cover structure is coded in cover and dists.
    [numpts,NL] = size(dists);
    barys = zeros(numpts,NL);
    % assign weights
    for j = 1:NL
        for i = cover{j}
            barys(i,j) = kernel(dists(i,j),radius);
        end
    end
    % renormalize weights
    for i = 1:numpts
        totali = sum(barys(i,:));
        if totali > 0
            barys(i,:) = barys(i,:)/totali;
        end
    end
end

