function [pts,numpts,dim,labels,base,circ,imgs] = load_edges_2()
    raw = load('../Datasets/Edges/edges_gen_5000.mat');
    pts = raw.data;
    base = raw.base;
    circ = raw.circ;
    labels = raw.labs;
    imgs = raw.raw;
    dim = size(pts,2);
    numpts = size(pts,1);
    return