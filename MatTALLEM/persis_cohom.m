function [Is_Z97,Is_Z2] = persis_cohom(pts, NL, maxdim)
%PERSIS_COHOM performs and views persistent cohomology on a dataset
% first finds landmarks via MM_geo, then uses that to find the PH^k. 
[landmarks,~,~,~,~,~,~,~,Dmat] = MM_geodesic(pts,NL,20,1,1);
Dmat_L = Dmat(landmarks,:);

Is_Z97 = ripserDM(Dmat_L,97,maxdim,Inf);
Is_Z2 = ripserDM(Dmat_L,2,maxdim,Inf);

% Reorder in terms of persistence. (largest Persis first)
for i = 1:maxdim+1
    Ps = Is_Z97{i}(:,2) - Is_Z97{i}(:,1);
    [~,order] = sort(-Ps);
    Is_Z97{i} = Is_Z97{i}(order,:);
    Ps = Is_Z2{i}(:,2) - Is_Z2{i}(:,1);
    [~,order] = sort(-Ps);
    Is_Z2{i} = Is_Z2{i}(order,:);
end

figure;
hold on
plotDGM(Is_Z97{1}, 'b');
plotDGM(Is_Z97{2}, 'r');
if maxdim == 2
    plotDGM(Is_Z97{3}, 'g');
end
hold off
figure;
hold on
plotDGM(Is_Z2{1}, 'b');
plotDGM(Is_Z2{2}, 'r');
if maxdim == 2
    plotDGM(Is_Z97{3}, 'g');
end
hold off


end