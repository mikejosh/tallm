function [pts,numpts,dim,labels,base,circ] = load_edges()
    % Maybe isomap on #27?
    raw = load('../Datasets/Edges/edges_data_final.mat');
    pts = raw.data;
    base = raw.base;
    circ = raw.circ';
    labels = raw.labs';
    dim = size(pts,2);
    numpts = size(pts,1);
    return