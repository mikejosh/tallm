function [val,der] = huber(input,bb)
%HUBER loss mollifier used for trace norm optimization.
%   Accepts vector input. Also yields the derivative. Assumes pos input.
%   This version is rescaled so that the limiting slope is always 1. 
    len = size(input,1);
    val = zeros(len,1);
    der = zeros(len,1);

    for i = 1:len
        if input(i) < bb
            val(i) = (input(i)^2)/(bb*2);
            der(i) = input(i)/bb;
        else
            val(i) = input(i)-bb/2;
            der(i) = 1; % (inaccuracte if bb = 0 and input(i) = 0)
        end
    end
end

