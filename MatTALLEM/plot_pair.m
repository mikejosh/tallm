function [] = plot_pair(models, Omega, alpha ,cover, ii, jj)
% Plots a pair of models in the same alignment space in different colors.
    set1 = cover{ii};
    set2 = cover{jj};
    [~,SS1,SS2] = intersect(set1,set2);
    X1 = models{ii}(SS1,:);
    X2 = models{jj}(SS2,:);
    pts1 = X1;
    pts2 = X2*Omega{ii,jj} + alpha{ii,jj};
    diffs = pts1-pts2;
    scatter3(diffs(:,1),diffs(:,2),diffs(:,3));
    disp(mean(diffs));
end