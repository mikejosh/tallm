function [adjsG,adjsT] = define_MST(cover)
%SPANTREE Define a spantree for the nerve complex which favors larger
%intersection sizes. (And thus is defined from the cover sets themselves).
    % Step 1: define a weighted adjacency matrix to define the graph.
    NV = length(cover);
    adjsG = zeros(NV,NV);
    for ii = 1:NV
        Si = cover{ii};
        for jj = (ii+1):NV
            Sj = cover{jj};
            val = length(intersect(Si,Sj));
            if val ~= 0
                adjsG(ii,jj) = 1/val;
                adjsG(jj,ii) = 1/val;
            end
        end
    end
    G = graph(adjsG);
    T = minspantree(G);
    adjsT = full(adjacency(T));
end

