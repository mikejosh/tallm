function [images] = image_prep(pts,dim1,dim2,flip)
%IMAGE_PREP prepares points as images in the format needed for plotting.
  
    if ~exist('flip', 'var') || isempty(flip)
        flip = false;
    else
        flip = true;
    end

    [numpts,dim] = size(pts);
    if dim ~= dim1*dim2
        images = [];
        disp('Given dimensions do not match');
        return
    end
    images = zeros(dim1,dim2,numpts);
    for i = 1:numpts
        if flip
            images(:,:,i) = reshape(pts(i,:),dim2,dim1)';            
        else
            images(:,:,i) = reshape(pts(i,:),dim1,dim2);
        end
    end
end

