function [pts,dim,labels] = load_faces(numpts,RF)
    face = load('../Datasets/frey_rawface.mat');
    face = (face.ff)';
    face = im2double(face);
    minpts = size(face,1);
    numpts = min([minpts,numpts]);
    if RF == 2
       dim = 560/4;
    elseif RF == 4
        dim = 560/16;
    else
        dim = 560;
    end
    
    pts = zeros(numpts,dim);
    ss = sort(randsample(minpts,numpts,false));
    for i = 1:numpts
        pts(i,:) = crunch_image(face(ss(i),:),20,28,RF);
    end
    labels = ss/max(ss);