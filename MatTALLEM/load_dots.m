function [pts,numpts,dim,labels] = load_dots(trims)
    raw = load('../Datasets/Image_Dots/ex_image_dots.mat');
    pts = raw.data';
    [numpts,dim] = size(pts);  
    loci = zeros(numpts,2);
    angles = zeros(numpts,2);
    labels = zeros(numpts,3);
    [~,pp] = max(pts,[],2);
    for i = 1:numpts
        p2 = mod(pp(i)-1,17)+1;
        loci(i,:) = [(pp(i) - p2)/17+1,p2];
    end
        
    for i = 1:numpts
        a1 = atan2(loci(i,1)-9,loci(i,2)-9);
        d = max(abs(loci(i,:)-9));
        a2 = pi*d/9.5;
        angles(i,:) = [a1,a2];
        labels(i,:) = [sin(a2)*cos(a1),sin(a2)*sin(a1),cos(a2)];
    end
    for i = 1:trims
        pts = imtrim(pts,19-2*i,19-2*i);
    end
    dim = (17-2*trims)^2;
    
    return