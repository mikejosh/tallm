function [Os,indexes] = Collect_Alignments(Omega)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    NV = size(Omega,1);
    d = size(Omega{1,1},1);
    nO = 0;
    Os = zeros(NV*NV,d*d); % allocate max length.
    indexes = zeros(NV*NV,2); % allocate max length.
    for ii = 1:NV
        for jj = 1:NV
            Oij = Omega{ii,jj};
            if ~isempty(Oij)
                nO = nO + 1;
                Os(nO,:) = reshape(Oij,d*d,1);
                indexes(nO,:) = [ii,jj];
            end
        end
    end
    Os = Os(1:nO,:);
    indexes = indexes(1:nO,:);
end

