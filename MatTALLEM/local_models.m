function [models] = local_models(pts,cover,localtype,localdim)
%LOCAL_MODELS Finds local models based on cover and local type.
%   Either PCA or ISOMAP on each coverset.    
    if strcmp(localtype,'circ')
        localdim = 2;
    end
    
    NL = size(cover,1);
    models = cell(NL,1);
    for j = 1:NL
        disp(['Finding model number ' num2str(j) '/' num2str(NL) '...'])
        X = pts(cover{j},:);
        if strcmp(localtype,'pca')
            Y = pca(X,localdim);
        elseif strcmp(localtype,'iso')
            Y = isomap(X,localdim);
        elseif strcmp(localtype,'pcacirc')
            Y = pca(X,localdim);
            for i = 1:size(Y,1)
                Y(i,:) = Y(i,:)/norm(Y(i,:));
            end
        elseif strcmp(localtype,'isocirc')
            Y = isomap(X,localdim);
            for i = 1:size(Y,1)
                Y(i,:) = Y(i,:)/norm(Y(i,:));
            end
        elseif strcmp(localtype,'circ')
            localnum = size(X,1);
            localNV = min(200, round(localnum/5));
            [LLs,Lrad] = FPS(X,localNV);
            Y = CM_torus_coords(X,LLs,1.5*Lrad,1);
        end
        models{j} = Y;
    end
    
end

