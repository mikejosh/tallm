function [dCov] = Distance_Covariance_DM(DX,DY)
% DISTANCE_COVARIANCE_DM computes the distance covariance between two
% distance matrices DX and DY (square, same size), with identical indexing.
    numpts = size(DX,1);
    mvecX = mean(DX);
    mvecY = mean(DY);
    meanX = mean(mvecX);
    meanY = mean(mvecY);
    
    % Double center the distance matrices.
    for ii = 1:numpts
        for jj = 1:numpts
            DX(ii,jj) = DX(ii,jj) - mvecX(ii) - mvecX(jj) + meanX;
            DY(ii,jj) = DY(ii,jj) - mvecY(ii) - mvecY(jj) + meanY;
        end
    end
    
    % Usual Covariance under double sum:
    dCov = 0;
    for ii = 1:numpts
        for jj = 1:numpts
            dCov = dCov + DX(ii,jj)*DY(ii,jj);
        end
    end
    dCov = dCov/(numpts^2);
    
    
end
