function [frame] = frame_multiproject(frame,As,bottom)
%FRAME_MULTIPROJECT Projects frames onto span(A) for each A in As.
%   This is used to find the data frames on-demand.
%   More specifically, we take SVD of AtFn = USVt, then U(I;O)Vt yields
%   the desired new frame. 
    numA = length(As) - bottom;
    d = size(frame,2); %(last dimension in A)
    for i = 1:numA
        A = As{i};
        [U,S,V] = svd(A'*frame);
        for j = 1:d
            S(j,j) = 1;
        end
        frame = U*(S*V');
   end
end

