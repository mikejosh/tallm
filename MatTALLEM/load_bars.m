function [pts,dim,labels] = load_bars(numpts,BW,aspect)
    dim = aspect^2;
    pts = zeros(numpts,dim);
    
    angles = 1*pi*(rand(numpts,1)-1/2);
    offset = 2*BW*(rand(numpts,1)-1/2);
    labels = [angles,offset];
    XYvals = -1:1/(aspect-1):1;
    
    for j = 1:numpts
        vec2 = [sin(angles(j)),cos(angles(j))];
        for i = 1:dim
            xval = XYvals(mod(i-1,aspect)+1);
            yval = XYvals(floor((i-1)/aspect)+1);
            vec1 = [xval,yval];
            dist = dot(vec1,vec2) - offset(j);
            pts(j,i) = 1/(1+exp(dist^2));
        end
    end
    
    return