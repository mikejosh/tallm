function [newpts,newdim] = imtrim(pts,dim1,dim2)

[numpts,dim] = size(pts);
newdim1 = dim1 - 2;
newdim2 = dim2 - 2;
newdim = newdim1*newdim2;
newpts = zeros(numpts,newdim);

for i = 1:numpts
    pt = pts(i,:);
    pt = reshape(pt,dim1,dim2);
    pt = pt(2:dim1-1,2:dim2-1);
    newpts(i,:) = reshape(pt,newdim,1);
end

return