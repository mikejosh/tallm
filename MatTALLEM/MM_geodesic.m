function [landmarks,rad1,rad2,cover1,cover2,assign1,assign2,PoU,Dmat] = MM_geodesic(pts,NL,k,THR1,THR2)
%MM_GEODESIC combined function which goers through several steps.
%   First, find landmarks and geodesic distances to the landmarks.
%   Next, create 2 covers from those landmarks via a pair of radius ratios.
%   Finally, define a partition of unity from the smaller cover. 
%   This is done specially due to the special code for using recorded 
%   geodesic distances

    numpts = size(pts,1);
    [nbhds,dists] = knnsearch(pts,pts,'k',k+1);
    nbhds(:,1) = []; % Remove self-neighbor.
    dists(:,1) = []; % Remove self-neighbor.
    
    % Sparse matrix encodes the symmetric kNN - graph.
    Gmat = spalloc(numpts,numpts,2*numpts*k);
    for i = 1:numpts
        for jj = 1:k
            j = nbhds(i,jj);
            dist = dists(i,jj);
            Gmat(i,j) = dist;
            Gmat(j,i) = dist;
        end
    end
    
    landmarks = zeros(NL,1);
    Dmat = zeros(numpts,NL);
    next = 1;
    landmarks(1) = next;
    Dmat(:,1) = dijkstra(Gmat,next)';
        
    for nl = 2:NL
        [~,next] = max(min(Dmat(:,1:(nl-1)),[],2));
        landmarks(nl) = next;
        Dmat(:,nl) = dijkstra(Gmat,next)';
    end
    [radius,~] = max(min(Dmat,[],2));
    
    rad1 = THR1 * radius; % Large rad. (local models and procrustes)
    rad2 = THR2 * radius; % Small rad. (PoU)
    
    %%% ---------- LANDMARK COVER ---------- %%%
    coversizes = zeros(NL,2);
    
    locs1 = Dmat < rad1;
    locs2 = Dmat < rad2;
    
    coversizes(:,1) = sum(locs1,1);
    coversizes(:,2) = sum(locs2,1);
    
    cover1 = cell(NL,1);
    cover2 = cell(NL,1);
    assign1 = cell(numpts,1);
    assign2 = cell(numpts,1);
    for j = 1:NL
        cover1{j} = find(locs1(:,j));
        cover2{j} = find(locs2(:,j));
    end
    for i = 1:numpts
        assign1{i} = find(locs1(i,:));
        assign2{i} = find(locs2(i,:));
    end
    
    
    mus = mean(coversizes,1);
    sds = std(coversizes,1);
    cms = min(coversizes,[],1);
    cMs = max(coversizes,[],1);
    mu1 = num2str(mus(1));
    mu2 = num2str(mus(2));
    sd1 = num2str(sds(1));
    sd2 = num2str(sds(2));
    cm1 = num2str(cms(1));
    cm2 = num2str(cms(2));
    cM1 = num2str(cMs(1));
    cM2 = num2str(cMs(2));

    disp(['Cover1 sizes in [',cm1,',',cM1,'] mean ',mu1,' +/- ',sd1])
    disp(['Cover2 sizes in [',cm2,',',cM2,'] mean ',mu2,' +/- ',sd2])
    
    %%% ---------- Partition of Unity (barycentric coords) ---------- %%%
    % Only computed for cover2 and rad2.
    
    PoU = zeros(numpts,NL);
    for i = 1:numpts
        for j = assign2{i}
            PoU(i,j) = kernel(Dmat(i,j),rad2);
        end
        PoU(i,:) = PoU(i,:)/sum(PoU(i,:));
    end
    
end

