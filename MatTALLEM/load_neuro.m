function [pts,numpts,dim,labels] = load_neuro()
    % Maybe isomap on #27?
    raw = load('../Datasets/Neuro/Dot_Field/Moving_dot_field_spike_counts_ma027_ip.mat');
    data = raw.trial_spike_count;
    labels = [raw.trial_stimulus.dot_field_direction,raw.trial_stimulus.dot_field_speed];
    dim = size(data,1)*size(data,3);
    numpts = size(data,2);
    pts = zeros(numpts,dim);
    for index = 1:size(data,3)
        pts(:,(index-1)*size(data,1)+1:index*size(data,1)) = data(:,:,index)';
    end    
    return