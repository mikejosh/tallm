function [Omega,alpha] = model_align(cover,edges,models)
%MODEL_ALIGN Procrustes alignment on all the relevant covers.
% find model alignment based on cover1 indexes in models whenever
% cover2 sets intersect. Also returns the simps in nerve(cover2)
    
    NE = size(edges,1);
    NL = size(cover,1);
    Omega = cell(NL);
    alpha = cell(NL);
    
    for ne = 1:NE
        i = edges(ne,1);
        j = edges(ne,2);
        set1 = cover{i};
        set2 = cover{j};
        [~,SS1,SS2] = intersect(set1,set2);
        X1 = models{i}(SS1,:);
        X2 = models{j}(SS2,:);
        % multiplication on the right aligns ([X2*(M.T)+M.c]~X1)
        [~,~,M12] = procrustes(X1,X2,'scaling',false);
        [~,~,M21] = procrustes(X2,X1,'scaling',false);
        Omega{i,j} = M12.T;      % For operation on the right.
        alpha{i,j} = M12.c(1,:);
        Omega{j,i} = M21.T;      % For operation on the right.
        alpha{j,i} = M21.c(1,:);
    end
    
    localdim = size(models{1},2);
    
    % 
    for nl = 1:NL
        Omega{nl,nl} = eye(localdim);
        alpha{nl,nl} = zeros(1,localdim);
    end

end

