function [F,GF] = beta_nuclear(A,frames,bb)
%BETA_NUCLEAR Beta-mollified nuclear norm. 
%   Computes the beta-mollified nuclear norms for (a sample of?) the given
%   frames. To be used as input to the PCAL_BR function. 
%   beta is the level of mollification, while frames is the dataset of
%   frames which one is trying to reduce.
%   The dimensions K and D are implicitly determined by the input A.

    [n,d] = size(frames{1});
    numpts = n/d;
    %subset = sort(randsample(numpts,300,false));
    subset = 1:numpts;
    F = 0;
    GF = zeros(size(A));
    for i = subset
        % Compute SVD for eval, b/c it helps for deriv.
        [U,S,V] = svd((A')*frames{i});
        [ssb,ssd] = huber(diag(S),bb);
        F = F - sum(ssb);

        % Huber-weighted gradient.
        for k = 1:d
            S(k,k) = ssd(k);
        end
        G = U*S*V';
        
        % chain rule collapses in k-direction via delta_jm, 
        % yielding a matrix product along the remaining d-directions. 
        GF = GF - frames{i}*G';
    end

end

