function [dist] = KB_distance(pt1,pt2)
%KB_DISTANCE evaluates distance on a klein bottle using intrinsic
%coordinates in [-pi,pi]^2
    x1 = pt1(1);
    y1 = pt1(2);
    x2 = pt2(1);
    y2 = pt2(2);
    d1 = norm(pt1-pt2);
    d2 = norm([x1-x2,2*pi-abs(y1-y2)]);
    d3 = norm([2*pi-abs(x1-x2),y1+y2]);
    d4 = norm([2*pi-abs(x1-x2),2*pi-abs(y1+y2)]);
    dist = min([d1,d2,d3,d4]);
end

