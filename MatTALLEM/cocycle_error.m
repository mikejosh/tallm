function [errs] = cocycle_error(Omega,triples)
%COCYCLE_ERROR computes approx. cocycle error from Procrustes alignment.
%   Specifically, we consider Omega_ij*Omega_jk ~ Omega_ik.
    
    NT = size(triples,1);
    errs = zeros(NT,1);
    for nt = 1:NT
        T = triples(nt,:);
        O1 = Omega{T(1),T(2)};
        O2 = Omega{T(2),T(3)};
        O3 = Omega{T(1),T(3)};
        errs(nt) = norm(O1'*O2'-O3');
    end
end

