function [coords] = global_coords(Omega,models,offsets,cover1,assign2,PoU,As,indexes, bottom)
%LOCAL_COORDS Yields local coordinates for the points in terms of the
%original models, but aligned via Omegas and offsets. 
%   take i = index(x)
%   F(x) = sum_j phi_j(x) Phi_j(x) (f_j(x) + tau_j)
% Accomplishing this summation requires forming and reducing ALL of the
% frames, and is likely to be VERY time consuming.
% Phi_j(x) can be determined via frame multiproject.
    
    numpts = size(PoU,1);
    d = size(models{1},2);
    K = size(As{size(As,1)-bottom},2);
    J = size(Omega,1);
    coords = zeros(numpts,K);
    disp('Computing global TALLEM coordinates:')
    for x = 1:numpts
        if mod(x,numpts/40) < mod(x-1,numpts/40)
            if mod(x,numpts/10) < mod(x-1,numpts/10)
                fprintf('+')
            else
                fprintf('-')
            end
        end
        ii = indexes(x);
        for j = assign2{x}
            frame = zeros(d*J,d);
            % only compute where PoU > 0 **and thus Omega_{ji} exists**
            for i = assign2{x}
                wgt = sqrt(PoU(x,i));
                frame(d*(i-1)+1:d*i,:) = wgt*Omega{i,j}';
            end
            frame = frame_multiproject(frame,As,bottom);
            k = quickfind(cover1{j},x);
            gjx = models{j}(k,:) + offsets(j,:);            
            coords(x,:) = coords(x,:) + PoU(x,j)*gjx*frame';
        end
    end
    disp('')
end

