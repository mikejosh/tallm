function [CCs,RmZs,tau,theta,barys] = CM_torus_coords(pts,landmarks,min_cover_rad,outdim)
%CM_TORUS_COORDS is the top-level circular coordinates function. 
% For now, we will fix weights in terms of distance and euclidean distance
% functions. 

% Used for choosing a moderate radius for CCs.
    TL = 0.25;
    TU = 0.99;

    q = 97;
    %%% cocycle prep %%%
    [Is,Cs] = ripserPC(pts(landmarks,:),q,1,Inf);
    features = Is{2};
    persis = features(:,2) - features(:,1);
    [~,order] = sort(-persis);
    longest = order(1:outdim);
    intervals = features(longest,:);
    raw_cocycles = cell(outdim,1);
    for i = 1:outdim
        j = longest(i);
        raw_cocycles{i} = Cs{2}{j};
    end
    % Determine the radius so that it lies between common birth and death,
    % and so that the given radius provides a cover of the dataset.
    fullbirth = max(intervals(:,1));
    fulldeath = min(intervals(:,2));
    min_param = fullbirth * (1-TL) + fulldeath*TL;
    max_param = fullbirth * (1-TU) + fulldeath*TU;
    rad_param = 2*min_cover_rad;
    new_param = min(max(rad_param,min_param),max_param);
    radius = new_param/2;
    % Finding coverset assignments, and barycentric coords for later.
    disp("Finding Cover Data for Torus Coords")
    [cover,assign,dists] = landmark_cover(pts,landmarks,radius);
    barys = bary(dists,cover,radius);
    % induce each cocycle from Zq to Z and express as matrix over landmarks.
    % CCstack is a stack of circle cocycles. 
    NL = length(landmarks);
    CCstack = zeros(outdim,NL,NL);
    for slot = 1:outdim
        cocycle = raw_cocycles{slot};
        ne = length(cocycle);
        for ee = 1:ne
            ii = cocycle(ee,1)+1; % switch from C++ to MATLAB indexes.
            jj = cocycle(ee,2)+1; % Switch from C++ to MATLAB indexes.
            val = cocycle(ee,3);
            if val <= (q-1)/2
                CCstack(slot,ii,jj) = val - 0;
                CCstack(slot,jj,ii) = 0 - val;
            else
                CCstack(slot,ii,jj) = val - q;
                CCstack(slot,jj,ii) = q - val;
            end
        end
    end
    
    %% incorporate cocycle check in matlab?
    % if verbose: 
    %     for slot in range(outdim):
    %         print(cocycle_check_Z(simps[2],CCstack[slot]))
    
    % Rephrase the cocycle as function on the simplices *as ordered in simps*.
    % This simplex-index-based ordering will guide the linear algebra.
    simps = nerve(cover);
    NV = length(simps{1});
    NE = length(simps{2});
    eta = zeros(NE,outdim);
    for index = 1:NE
        edge = simps{2}(index,:);
        eta(index,:) = CCstack(:,edge(1),edge(2));
    end

    %%% weight-adjusted cobdry inverse %%%
    % Diagonal Weight Matrices:
    Wv = ones(NV,1);
    We = zeros(NE,1);
    for index = 1:NE
        edge = simps{2}(index,:);
        pt1 = pts(landmarks(edge(1)),:);
        pt2 = pts(landmarks(edge(2)),:);
        dist = norm(pt1-pt2);
        val = kernel(dist,2*radius);
        We(index) = val;
    end
    % Coboundary, reweighted. [NE x NV]: C^0 -> C^1
    % Adjust to ONB, P-I, then readjust to simplex bases.
    % Note: we may have been weighting these upside down...
    delta = delta0(simps);
    deltaW = diag(sqrt(We)) * delta * diag(1./sqrt(Wv));
    deltaWI = pinv(deltaW);
    deltaI = diag(1./sqrt(Wv)) * deltaWI * diag(sqrt(We));
    
    % Now derive the harmonic pieces: eta = NE x dim; tau = NV x dim;
    % Theta is Eta, but adjusted by the appropriate coboundary.
    tau = -deltaI*eta;
    thetavec = eta + delta*tau;
    theta = zeros(outdim,NV,NV);
    for index = 1:NE
        edge = simps{2}(index,:);
        theta(:,edge(1),edge(2)) = (+1)*thetavec(index,:);
        theta(:,edge(2),edge(1)) = (-1)*thetavec(index,:);
    end
    
    % Now to evaluate the coordinates:
    numpts = size(pts,1);
    CCs = zeros(numpts,2*outdim);
    RmZs = zeros(numpts,outdim);
    for slot = 1:outdim
        for ii = 1:numpts
            jj = assign{ii}(1);
            val = tau(jj,slot);
            for kk = assign{ii}'
                val = val + barys(ii,kk)*theta(slot,kk,jj);
            end
            angle = 2*pi*val;
            RmZs(ii,slot) = mod(val,1);
            CCs(ii,(2*slot-1):(2*slot)) = [cos(angle),sin(angle)];
        end
    end
end