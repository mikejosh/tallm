function [coords] = AGA_coords(Omega,models,offsets,cover1,PoU)
%UNTITLED Almost Globally Aligned Coordinates from reduced transition
%functions {OmegaCs}.
%   These are smoothed local coordinates in d-dimentions, which are built
%   to defined by an appropriate cocycle which is as close as possible to
%   the MST realigned coordinates. In particular, this new cocycle is unity
%   on most edges, and only takes a few values on other edges: the number
%   of potential alternate values depends largely upon the topology of the
%   base coordinates [in particular dim (H^1)]. 

    localdim = size(models{1},2);
    numpts = size(PoU,1);
    NV = size(PoU,2);
    
    coords = zeros(numpts,localdim);
    IDs = zeros(numpts,1);
    
    for n = 1:numpts
        [~,i] = max(PoU(n,:));
        IDs(n) = i;
    end
    
    for j = 1:NV
        NC = size(cover1{j},1);
        for slot = 1:NC
            n = cover1{j}(slot);
            i = IDs(n);
            gjx = models{j}(slot,:) + offsets(j,:);
            coords(n,:) = coords(n,:) + PoU(n,j)*gjx*Omega{i,j};
        end
    end
        
end

