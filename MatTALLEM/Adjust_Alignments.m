function [Omega,offsets] = Adjust_Alignments(models,cover,simps,edges,IDs,centers)
%UNTITLED2 Create new averaged alpha and Omega.
%   Omega: cocycle to start with. (immediately replaced)
%   
    nO = size(edges,1);
    NV = length(cover);
    Omega = cell(NV);
    alpha = cell(NV);
    for k = 1:nO
        % First, set Omega
        edge = edges(k,:);
        ii = edge(1);
        jj = edge(2);
        ID = IDs(k);
        W = centers(:,:,ID);
        Omega{ii,jj} = W;
        % Next, set alpha to optimally align:
        Si = cover{ii};
        Sj = cover{jj};
        [~,SSi,SSj] = intersect(Si,Sj);
        Xi = models{ii}(SSi,:);
        Xj = models{jj}(SSj,:);
        Mi = mean(Xi);
        Mj = mean(Xj);
        alpha{ii,jj} = Mi - Mj*W;
    end
    
    % Finish alignment for diagonal.
    localdim = size(models{1},2);
    for v = 1:NV
        Omega{v,v} = eye(localdim);
        alpha{v,v} = zeros(1,localdim);
    end
    
   % Finally, grab offsets:
   offsets = align_offsets(Omega,alpha,simps);
end

