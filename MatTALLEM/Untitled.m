function [newcoords] = AGA_coords(OmegaCs,indexes,)
%UNTITLED Almost Globally Aligned Coordinates from reduced transition
%functions {OmegaCs}.
%   These are smoothed local coordinates in d-dimentions, which are built
%   to defined by an appropriate cocycle which is as close as possible to
%   the MST realigned coordinates. In particular, this new cocycle is unity
%   on most edges, and only takes a few values on other edges: the number
%   of potential alternate values depends largely upon the topology of the
%   base coordinates [in particular dim (H^1)]. 
    localdim = length(alpha
end

