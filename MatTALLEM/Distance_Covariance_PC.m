function [dCov] = Distance_Covariance_PC(X,Y)
% DISTANCE_COVARIANCE_PC computes the distance covariance between two
% datasets X and Y in Euclidean metric (same indexing)
    numpts = size(X,1);
    DmatX = zeros(numpts);
    DmatY = zeros(numpts);
    for ii = 1:numpts
        for jj = 1:numpts
            DmatX(ii,jj) = norm(X(ii,:) - X(jj,:));
            DmatY(ii,jj) = norm(Y(ii,:) - Y(jj,:));
        end
    end
    
    dCov = Distance_Covariance_DM(DmatX,DmatY);
end

