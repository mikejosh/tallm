function newvec = crunch_image(vec,dim1,dim2,RF)
%CRUNCH_IMAGE reduces an image by the reduction factor RF.
%   Each new pixel is the mean in the original image over an RF x RF box.
    if RF > 1 && mod(dim1,RF) == 0 && mod(dim2,RF) == 0 && length(vec) == dim1*dim2
        newdim1 = dim1/RF;
        newdim2 = dim2/RF;
        im = reshape(vec,dim1,dim2);
        im2 = zeros(newdim1,newdim2);
        for ii = 1:newdim1
            for jj = 1:newdim2
                for kk = 0:RF-1
                    for ll = 0:RF-1
                        im2(ii,jj) = im2(ii,jj) + im(ii+kk,jj+ll);
                    end
                end
            end
        end
        newvec = reshape(im2,newdim1*newdim2,1);
    else
        newvec = vec;
    end
return