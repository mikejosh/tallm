function [offsets] = align_offsets(Omegas,alphas,simps)
% ALIGN_OFFSETS yields an Omega-weighted couboundary tau in C^0(N,R^d)
    % main task: orient things, assuming orths are (very nearly?) a cocycle.
    dim = size(Omegas{1,1},1);
    verts = simps{1};
    edges = simps{2};
    NV = size(verts,1);
    NE = size(edges,1);
    delta = zeros(dim*NE,dim*NV);
    for index = 1:NE
        v1 = edges(index,1);
        v2 = edges(index,2);
        delta(dim*(index-1)+1:dim*index,dim*(v2-1)+1:dim*v2) = Omegas{v1,v2};
        delta(dim*(index-1)+1:dim*index,dim*(v1-1)+1:dim*v1) = -eye(dim);
    end

    deltaX = pinv(delta); %MP pseudo-inverse of augmented coboundary
    shiftE = zeros(dim*NE,1);
    for index = 1:NE
        v1 = edges(index,1);
        v2 = edges(index,2);
        shiftE(dim*(index-1)+1:dim*index) = alphas{v1,v2};
    end
    shiftV = deltaX*shiftE;
    offsets = zeros(NV,dim);
    for index = 1:NV
        offsets(index,:) = shiftV(dim*(index-1)+1:dim*index);
    end
end

