function [A_0,M] = frame_init(frames, K, verbose)
%FRAME_INIT finds an initial K-frame for PCAL_BR optimization.
%   Finds the zero-mean PCA for the collection of frame VECTORS by
%   concatenating all of the frames into a giant (dN x dJ) matrix, Fbar.
%   Then, we find the eigendecomposition for (dJ x dJ) (Fbar^T)*Fbar.
%   The top K eigenvectors correspond to the initial K-frame A. 
%   By construction, this is a matrix in V_K(R^dJ).
    if ~exist('verbose', 'var')
        verbose = false;
    end
    N = size(frames,1);
    d = size(frames{1},2);
    J = size(frames{1},1)/d;
    
    Fbar = zeros(d*N,d*J);
    for i = 1:N
        Fbar((i-1)*d+1:i*d,:) = frames{i}';
    end
    M = (Fbar')*Fbar;
    [vecs,D] = eig(M);
    vals = diag(D)/(d*N);
    [~,order] = sort(-vals);
    
    if verbose
        Cvals = zeros(length(order),1);
        Cvals(1) = vals(order(1));
        for i = 2:length(order)
            Cvals(i) = Cvals(i-1) + vals(order(i));
        end
        plot(Cvals)
    end
    
    A_0 = vecs(:,order(1:K));
end

