function [coords] = local_coords(indexes,Omegas,models,offsets,cover1,assign2,PoU)
%LOCAL_COORDS Yields local coordinates for the points in terms of the
%original models, but aligned via Omegas and offsets. 
%   This function only makes sense if Omega is a cocycle.
%   Thus, global_coords is used instead.
%   If Omega is a cocycle, then take i = index(x), and
%   G_i(x) = sum_{j:x in Xj} phi_j(x) Omegas_{i,j}'[g_j(x)+T_j]
%   has Omega{l,i}G_i(x) = G_l(x) whenever x is in Xi and Xl. 

    numpts = size(PoU,1);
    dim = size(models{1},2);
    coords = zeros(numpts,dim);
    for x = 1:numpts
        i = indexes(x);
        for j = assign2{x}
            k = quickfind(cover1{j},x);
            gjx = (models{j}(k,:) + offsets(j,:));
            coords(x,:) = coords(x,:) + PoU(x,j)*gjx*Omegas{i,j};
        end
    end
end

