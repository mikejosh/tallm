function [pts,numpts,dim,labels] = load_octane()
    raw = load('../Datasets/Octane/data_example_cyclo-octane.mat');
    pts = [raw.X_sample, raw.landmarks]';
    [numpts,dim] = size(pts);
    labels = raw.data_iso.coords{3,1}';
    
    return