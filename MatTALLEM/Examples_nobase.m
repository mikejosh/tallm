%% Script for TALLEM without a base space and for localdim > 1.
% The MatTALLEM code also handles non-cocycle Omega.
ti = tic;
% Data Parameters:
dataset = 'FACE';       % FACE & NIST & DOTS & CYOC
numpts = 1965;          % 1965 & 2000 & 2000 & 6340
RF = 1;
BW = 0.4;
aspect = 17;

% TALLEM parameters:
localdim = 2;           % Local dimension.
Kfinal = 2;             % Final embedding dimension.
localtype = 'pca';      % Method for local coords.
THR1 = 2.5;             % Larger cover radius.
THR2 = 1.5;             % Smaller cover radius.
nn = 30;                % number of nearest neighbors for geodesic/isomap.
NL = 40;                 % Number of landmarks

% Stepwise functions.
if strcmp(dataset,'FACE')
    numpts = 1965;
    [pts,dim,labels] = load_faces(numpts,RF);
    dim1 = 28/RF;
    dim2 = 20/RF;
elseif strcmp(dataset,'MOBB')
    [pts,dim,labels] = load_bars(numpts,BW,aspect);
    dim1 = aspect;
    dim2 = aspect;
elseif strcmp(dataset,'NIST')
    numpts = 2000;
    [pts,dim,labels] = load_mnist(numpts, [8], RF);
    dim1 = 28/RF;
    dim2 = 28/RF;
elseif strcmp(dataset,'DOTS')
    [pts,numpts,dim,labels] = load_dots(1);
    %numpts = 1900;
    pts = pts(1:numpts,:);
    labels = labels(1:numpts,:);
    dim1 = 15;
    dim2 = 15;
elseif strcmp(dataset,'CYOC')
    [pts,numpts,dim,labels] = load_octane();
elseif strcmp(dataset,'ROLL')
    [pts,dim,labels] = load_roll(numpts,1,2,2,2,0);
    
end

%% Primary TALLEM Steps.

% Choose cover sets and PoU.
[landmarks,rad1,rad2,cover1,cover2,assign1,assign2,PoU,Dmat] = MM_geodesic(pts,NL,nn,THR1,THR2);

simps = nerve(cover2);
models = local_models(pts,cover1,localtype,localdim);
[Omega,alpha] = model_align(cover1,simps{2},models);
errs = cocycle_error(Omega,simps{3});
disp(['Maximum cocycle error is ',num2str(max(errs))])
[frames,indexes] = assemble_frames(assign2,PoU,Omega);
offsets = align_offsets(Omega,alpha,simps);

%% Prepare the parameters for frame reduction optimization.
% Note: we are doing in 1 step now, but we might need to iterate a few
% times to do it well (say diadically?)

opts = struct;
opts.M = []; % identity.
opts.xtol = 1e-10;
opts.gtol = 1e-6;
opts.ftol = 1e-12;
opts.proxparam = 0;
opts.stepsize = 0;
opts.solver = 1; % 1.PCAL  2.PLAM  3.PCAL-S?
opts.postorth = 1;
opts.info = 0; % verbosity?

opts.maxit = 2000;
betas = 0.99.^(1:2000);

% The solver is sensitive to this penalty parameter???
% Users need to tune different values with different problem settings.
opts.penalparam = 1;

%K = NL*localdim;
%Ks = zeros(K);
%numKs = 0;
%while K > Kfinal
%    numKs = numKs + 1;
%    K = max(Kfinal,floor(0.95*K));
%    Ks(numKs) = K;
%end
%Ks = Ks(1:numKs);
%Es = zeros(numpts,numKs);
%As = cell(numKs,1);

K = NL*localdim;
Ks = zeros(K);
Ks(1) = K;

% Initial drop?
numKs = 2;
K = max(Kfinal,floor(K/3));
Ks(numKs) = K;
% Followed by more gradual drops.
while K > Kfinal
    numKs = numKs + 1;
    K = max(Kfinal,floor(0.9*K));
    Ks(numKs) = K;
end
Ks = Ks(1:numKs);
Es = zeros(numpts,numKs);
As = cell(numKs,1);

%% iteratively reduce frame dimension via mollified PCAL.
disp("Other parts of TALLEM complete. Now projecting frames.")
for i = 1:numKs
    tic;
    K = Ks(i);
    [A_0,VAR] = frame_init(frames, K);
    [A,Out] = PCAL_BR(A_0, @beta_nuclear, betas, frames, opts);
    [frames,errors] = frame_project(frames,A);
    tt = toc;
    disp(['Projected frames down to K = ',num2str(K)])
    disp(['This projection took ',num2str(tt),' secs'])
    As{i} = A;
    Es(:,i) = errors;
end
%%

Y = global_coords(Omega,models,offsets,cover1,assign2,PoU,As,indexes,1);

%% View dataset image patches (if it makes sense)
Kfinal=3;
if ~strcmp(dataset,'ROLL') && ~strcmp(dataset,'CYOC')
    [ss,radius] = FPS(Y,200);
    %ss = randsample(numpts,200,false);
    %radius = 100;
    mask = zeros(numpts,1);
    mask(ss) = 1;
    ww = 1*radius;
    hh = ww;
    images = image_prep(pts,dim1,dim2,true);
    if Kfinal == 2
        imgScatter(Y(:,1:2), images, mask, ww, hh)
    elseif Kfinal == 3
        imgScatter3(Y(:,1:3), images, mask, ww, hh)
    end
else
    if Kfinal == 3
        lab = labels(:,1).^2 + labels(:,2).^2 + labels(:,3).^2;
        ss1 = lab > 7.2;
        ss2 = lab < 7.2;
        figure;
        scatter3(Y(:,1),Y(:,2),Y(:,3),15,(labels+3)/7)
        figure;
        scatter3(Y(ss1,1),Y(ss1,2),Y(ss1,3),15,(labels(ss1,:)+3)/7)
        figure;
        scatter3(Y(ss2,1),Y(ss2,2),Y(ss2,3),15,(labels(ss2,:)+3)/7)
    elseif Kfinal == 4
        ss1 = lab > 7.4;
        ss2 = lab < 7.4;
        figure;
        colormap jet;
        scatter3(Y(:,1),Y(:,2),Y(:,3),15,Y(:,4))
        figure;
        %scatter3(Y(ss1,1),Y(ss1,2),Y(ss1,3),15,exp(7.4-lab(ss1)))
        colormap jet;
        scatter3(Y(ss1,1),Y(ss1,2),Y(ss1,3),15,Y(ss1,4))
        figure;
        %scatter3(Y(ss2,1),Y(ss2,2),Y(ss2,3),15,exp(-7.1+lab(ss2)))
        colormap jet;
        scatter3(Y(ss2,1),Y(ss2,2),Y(ss2,3),15,Y(ss2,4))
    end
end
%%
%dens = zeros(numpts,1);
%idx = knnsearch(pts,pts,'K',7);
%for i = 1:numpts
%    dens(i) = 1/norm(pts(i,:) - pts(idx(i,7),:));
%end
%
%block = hsv(100);
%colormap(block(50:100,:));
%scatter3(Y(:,1),Y(:,2),Y(:,3),10,log(dens),'filled');
%disp(['Time for NL = ',num2str(NL),':'])
%toc(ti)