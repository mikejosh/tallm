%% Script for TALLEM on Klein Bottle Image Patches.
ti = tic;
% Data Parameters:
dataset = 'Edges';
localdim = 2;           % Local dimension.
Kfinal = 4;             % Final embedding dimension.
localtype = 'circ';     % Method for local coords.
THR1 = 4;               % Larger cover radius.
THR2 = 2.5;             % Smaller cover radius.
nn = 40;                % number of nearest neighbors for geodesic/isomap.
NL = 8;                 % Number of landmarks
verbose = max(min(round(10/NL),10),round(3/NL));

% NEURO EXAMPLE
%[oldpts,numpts,dim,labels] = load_neuro();
% Use PCA to denoise, then use isomap to obtain new coords.
%Xpca = pca(oldpts,20);
%pts = isomap(Xpca,20);
%basecoords = pts(:,1);
%colormap jet;
%scatter3(pts(:,1),pts(:,2),pts(:,3),20,labels(:,2))


% EDGES EXAMPLE
%[pts,numpts,dim,labels,basecoords,circ] = load_edges();
%[pts,numpts,dim,labels,~,circ,imgs] = load_edges_2();
numpts = 5000;
aspect = 5;
holesize = 1;
holedepth = 1.2;
[pts,dim,labels] = gen_klein_edges(numpts,aspect,holesize,holedepth);
% Define base mapping from circular coordinates
%%
Xpca = pca(pts,dim);
[circLs, min_cover_rad] = FPS(pts,500);
% This appears to be very slow in matlab...?
[CCs,RmZs] = CM_torus_coords(pts,circLs,min_cover_rad,1);
basecoords = CCs;
scatter3(Xpca(:,1),Xpca(:,2),Xpca(:,3),10,RmZs);
%% Primary TALLEM Steps.

% Choose cover sets and PoU.
[landmarks,rad1,rad2,cover1,cover2,assign1,assign2,PoU,Dmat] = ...
    MM_geodesic(basecoords,NL,nn,THR1,THR2);

simps = nerve(cover2);
models = local_models(pts,cover1,localtype,localdim);
[Omega,alpha] = model_align(cover1,simps{2},models);
errs = cocycle_error(Omega,simps{3});
disp(['Maximum cocycle error is ',num2str(max(errs))])
%%
[frames,indexes] = assemble_frames(assign2,PoU,Omega);
offsets = align_offsets(Omega,alpha,simps);
%%
[graph,spantree] = define_MST(cover2);
[alpha2,Omega2,models2] = spantree_alignment(pts,alpha,Omega,models,spantree,graph);
offset2 = align_offsets(Omega2,alpha2,simps);
% To check consistency of model changes and alpha2/Omega2, compare below:
%[Omega3,alpha3] = model_align(cover1,simps{2},models2);
err2 = cocycle_error(Omega2,simps{3});
disp(['Maximum cocycle error is ',num2str(max(err2))])
[Os,edges] = Collect_Alignments(Omega2);
Opca = pca(Os,localdim*localdim);
[IDs,C] = kmeans(Os,2); % This 2 depends on nerve topology? Use H^1?
centers = reshape(C',localdim,localdim,2); % same value...
% ***** should add a cocycle alignment/projection step here ***** %
[Omega3,offset3] = Adjust_Alignments(models2,cover1,simps,edges,IDs,centers);
err3 = cocycle_error(Omega3,simps{3});
disp(['Maximum cocycle error is ',num2str(max(err3))])
% These coords don't quite capture the boundary effect perfectly.
%[coords] = AGA_coords(Omega3,models2,offset3,cover1,PoU);
Orient = centers(:,:,2);
[coords,ToFlip] = AGA_circle(Omega3,models2,offset3,cover1,PoU,landmarks,RmZs,Orient);
coords = coords - mean(coords);
%% Now to switch from circular coords to 1D RmZs and plot:
localRmZs = atan2(coords(:,1),coords(:,2))/(2*pi);
%figure;
%scatter3(coords(:,1),coords(:,2),RmZs,10,labels(:,2));
%figure;
%scatter3(coords(:,1),coords(:,2),RmZs,10,labels(:,1));
figure;
scatter3(Opca(:,1),Opca(:,2),Opca(:,3),10,IDs);
figure;
scatter(localRmZs,RmZs,10,labels(:,2));
figure;
scatter(localRmZs,RmZs,10,labels(:,1));
%% Prepare the parameters for frame reduction optimization.

opts = struct;
opts.M = []; % identity.
opts.xtol = 1e-10;
opts.gtol = 1e-6;
opts.ftol = 1e-12;
opts.proxparam = 0;
opts.stepsize = 0;
opts.solver = 1; % 1.PCAL  2.PLAM  3.PCAL-S?
opts.postorth = 1;
opts.info = 0; % verbosity?

opts.maxit = 2000;
betas = 0.99.^(1:2000);

% The solver is sensitive to this penalty parameter???
% Users need to tune different values with different problem settings.
opts.penalparam = 1;

K = NL*localdim;
Ks = zeros(K);
Ks(1) = K;

% Initial drop?
numKs = 2;
    numKs = numKs + 1;
    K = max(Kfinal,floor(0.9*K));
K = max(Kfinal,floor(K/3));
Ks(numKs) = K;
% Followed by more gradual drops.
while K > Kfinal
    Ks(numKs) = K;
end
Ks = Ks(1:numKs);
Es = zeros(numpts,numKs);
As = cell(numKs,1);

%% iteratively reduce frame dimension via mollified PCAL.
disp("Other parts of TALLEM complete. Now projecting frames.")
for i = 1:numKs
    tic;
    K = Ks(i);
    % Can we use the eigvecs of var (sing vals of Frame Collection)
    % To iteratively choose the next K?
    % Seems quite gradual in change...
    [A_0,VAR] = frame_init(frames, K);
    [A,Out] = PCAL_BR(A_0, @beta_nuclear, betas, frames, opts);
    [frames,errors] = frame_project(frames,A);
    tt = toc;
    disp(['Projected frames down to K = ',num2str(K)])
    disp(['This projection took ',num2str(tt),' secs'])
    As{i} = A;
    Es(:,i) = errors;
end
disp(numKs);
%%
Y = global_coords(Omega,models,offsets,cover1,assign2,PoU,As,indexes,2);
%%
%Z = Y;
%%
Z = pca(Y,4);
colormap jet;
%scatter3(Y(:,1),Y(:,2),Y(:,3),20,Y(:,3))
scatter3(Z(:,1),Z(:,2),Z(:,3),20,Z(:,4))
%%
%Z = [basecoords,Y];
Z = pca(Y,5);
[Is_Z97,Is_Z2] = persis_cohom(Z, 400, 2);
%[Is_Z97,Is_Z2] = persis_cohom(pts, 400, 2);

%% Check matrix from all the way around:
%[B,I] = sort(circ(landmarks));
%mat = [1,0;0,1];
%for v=1:NL-1
%    disp(Omega{I(v),I(v+1)})
%    %*mat;
%end
%%mat = Omega{I(NL),I(1)}*mat;
%disp(mat)


%% (vec should utilize the offset)
%vec = (180*atan2(Z(:,2),Z(:,3))/pi+273-labels(:,1))/360; 
%noise = round(vec) - vec;
%scatter(1:1680,noise,20,labels(:,2))
%SD_Full = sqrt(var(noise))*2*pi
%SD_Fast = sqrt(sum(noise(labels(:,2)<20).^2)/sum(labels(:,2)<20))*2*pi

%% View dataset image patches (if it makes sense)
KBcoords = [RmZs,localRmZs];
%KBcoords = [labels(:,1),labels(:,2)];
[ss,radius] = FPS(KBcoords,100);
%ss = randsample(numpts,200,false);
%radius = 0.03;
mask = zeros(numpts,1);
mask(ss) = 1;
ww = 0.6*radius;
hh = ww;
images = image_prep(pts,aspect,aspect,true);
imgScatter(KBcoords(:,1:2), images, mask, ww, hh)