function [landmarks,radius] = FPS(X,NL)
%FPS or furthest point sampling. This code only works in Euclidean Space.
    numpts = size(X,1);
    Dmat = zeros(numpts);
    landmarks = zeros(NL,1);
    next = 1;
    landmarks(1) = next;
    for i = 1:numpts
        Dmat(next,i) = norm(X(i,:) - X(next,:));
    end
    
        
    for nl = 2:NL
        [~,next] = max(min(Dmat(landmarks(1:(nl-1)),:),[],1));
        landmarks(nl) = next;
        for i = 1:numpts
            Dmat(next,i) = norm(X(i,:) - X(next,:));
        end
    end
    [radius,~] = max(min(Dmat(landmarks(1:(nl-1)),:),[],1));
    
end

