function [frames,indexes] = assemble_frames(assign,PoU,Omega)
%ASSEMBLE_FRAMES Builds the assembly frames from the PoU and Omega.
% Phi_i(x) = [[ sqrt[phi_j(x)](Omega_ij) ]]_j
% Each frame is a dJ x d matrix, which acts as a map :R^d -> R^dJ by
% multiplication *on the left*.
% Transposed Omegas are used for this left action!
% NOTE: Omega_ii is always the identity map!

% Note that frames is very large: LARGER THAN numpts x d x dJ
% e.g. (10ish x 2000) x 2 x 200 = 800,000 for the mobius band example.

    [numpts,J] = size(PoU);
    frames = cell(numpts,1);    
    indexes = zeros(numpts,1);
    d = size(Omega{1,1},1);
    
    % Allocate all that memory! (sparse?)
    for i = 1:numpts
        frames{i} = zeros(d*J,d);
    end
    
    % Frames to guide for dim reduce
    for k = 1:numpts
        % choose the most reliable representative for x_k:
        [~,i] = max(PoU(k,:));
        indexes(k) = i;
        % only compute where PoU > 0 **and thus Omega_{ji} exists**
        for j = assign{k}
            wgt = sqrt(PoU(k,j));
            % Multiply on right by Omega{i,l} (nearly) yields F_k^l!
            frames{k}(d*(j-1)+1:d*j,:) = wgt*Omega{j,i}';
        end
    end
    
    % All the frames for implementing the Dim Reduce.
    %frames = cell(numpts,1);
    
    % Allocate all that memory! (sparse?)
    %for i = 1:numpts
    %    frames{i} = zeros(d*J,d);
    %end

    %for k = 1:numpts
    %    for i = assign{k}
    %        % only compute where PoU > 0 **and thus Omega_{ji} exists**
    %        for j = assign{k}
    %            wgt = sqrt(PoU(k,j));
    %            % Multiply on right by Omega{i,l} (nearly) yields F_k^l!
    %            frames{k}(d*(j-1)+1:d*j,:) = wgt*Omega{j,i};
    %        end
    %    end
    %end
end

