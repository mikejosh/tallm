function [delta] = delta0(simps)
%DELTA0. Gives coboundary matrix in terms and order of the input simps.
    verts = simps{1};
    edges = simps{2};
    NV = length(verts);
    NE = length(edges);
    delta = zeros(NE,NV);
    for index = 1:NE
        edge = edges(index,:);
        v1 = edge(1);
        v2 = edge(2);
        delta(index,v1) = 1;
        delta(index,v2) = -1;
    end
end

