function [pts,dim,labels] = load_mnist(ppc, classes, RF)
    % ppc = points per class
    % classes = list of numbers in 0,1,2,3,4,5,6,7,8,9.
    % 0 is in slot 10, hence the use of mods.
    % RF is the reduction factor, which utilizes reshaping.
    % 28 x 28 can be reduced by half to 14 x 14 if RF = 2, or 7x7 if RF = 4
            
    mnist_pts = load('../Datasets/mnist.mat');
    mnist_pts = mnist_pts.mnist_info;

    nC = length(classes);
    mnist_nums = zeros(nC,1);
    for j = 1:nC
        label = mod(classes(j)-1,10)+1;
        mnist_nums(j) = size(mnist_pts{label},1);
    end
    minsize = min(mnist_nums);
    ppc = min([ppc,minsize]);
    
    numpts = ppc*nC;
    if RF > 1 && mod(28,RF) == 0
        dim = (28/RF)^2;
    else
        dim = 28^2;
    end
    
    pts = zeros(numpts,dim);
    labels = zeros(numpts,1);
    
    for j = 1:nC
        label = mod(classes(j)-1,10)+1;
        labels((j-1)*ppc+1:j*ppc) = classes(j);
        ss = sort(randsample(mnist_nums(j),ppc,false));
        for i = 1:ppc
            k = i + (j-1)*(ppc);
            point = mnist_pts{label}(ss(i),:);
            pts(k,:) = crunch_image(point,28,28,RF);
        end
    end
    
    return