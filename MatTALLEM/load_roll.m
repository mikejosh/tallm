function [pts,dim,labels] = load_roll(numpts,spins,leng,n1,n2,holesize)
%LOAD_ROLL generates roll data (like the swiss roll), possibly with holes?

    if ~exist('holesize', 'var') || isempty(holesize)
        holesize = 0;
    end
    if holesize == 0
        nh = 0;
    else
        nh = n1*n2;
    end
    
    dim = 3;
    pts = zeros(numpts,dim);
    if nh == 0
        As = (2*pi)^2*(spins^2)*rand(numpts,1)+1;
        Bs = 2*pi*spins*leng*(rand(numpts,1)-1/2);
        labels = [As,Bs];
        As = sqrt(As); % close to isometric rolling.
        for i = 1:numpts
            a = As(i);
            b = Bs(i);
            pts(i,:) = [a*cos(a),a*sin(a),b];
        end
    else
        h1 = 0.5/n1:1/n1:1-0.5/n1;
        h2 = 0.5/n2:1/n2:1-0.5/n2;
        rad = min(holesize,1) * min(0.4/n1,0.4/n2);
        labels = zeros(numpts,2);
        for i = 1:numpts
            dist = 0;
            while dist < rad % until out of a hole.
                ab = rand(1,2);
                dist = 2*rad;
                for k = 1:n1
                    for l = 1:n2
                        dist = min(dist,norm(ab-[h1(k),h2(l)]));
                    end
                end
            end
            a = (2*pi)^2*(spins^2)*ab(1)+1;
            b = 2*pi*spins*leng*(ab(2)-1/2);
            labels(i,:) = [a,b];
            a = sqrt(a);
            pts(i,:) = [a*cos(a),a*sin(a),b];
        end
    end     
end