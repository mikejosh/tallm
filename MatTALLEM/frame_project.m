function [newframes,errors] = frame_project(frames,A)
%FRAME_PROJECT Projects frames onto span(A).
%   More specifically, we take SVD of AtFn = USVt, then U(I;O)Vt yields
%   the desired new frame. 
    numpts = size(frames,1);
    d = size(frames{1},2);
    newframes = cell(numpts,1);
    errors = zeros(numpts,1);
    for i = 1:numpts
        [U,S,V] = svd(A'*frames{i});
        for j = 1:d
            S(j,j) = 1;
        end
        newframes{i} = U*S*V';
        errors(i) = norm(frames{i}-A*newframes{i},'fro')^2;
    end
end

