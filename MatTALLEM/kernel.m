function [val] = kernel(dist,rad)
%KERNEL defines a cone kernel for use in TALLEM. Any other kernel may be
%   used instead. 
    val = max(1-dist/rad,0);
end

