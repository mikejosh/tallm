f = @(x,y,a,sigma)(exp(-(x*ones(1,size(a,2)) - ones(size(x,1),1)*a(1,:)).^2/sigma - (y*ones(1,size(a,2)) - ones(size(y,1),1)*a(2,:)).^2/sigma));

n_grid = 17;

[X_grid, Y_grid] = meshgrid(linspace(0,1,n_grid), linspace(0,1,n_grid));
X_grid = reshape(X_grid,[],1);
Y_grid = reshape(flipud(Y_grid),[],1);

n_data = 1900;

epsilon = 0.4;
a = (1 + epsilon)*rand(2,n_data) - epsilon/2;

% %a = 2*rand(3,n_data) -1;
% 
% t = linspace(-1,1,10);
% [x,y,z] = meshgrid(t,t,t);
% x = reshape(x,1,[],1);
% y = reshape(y,1,[],1);
% z = reshape(z,1,[],1);
% 
% [a1, a2, ar] = cart2sph(x,y,z);
% 
% % [a1, a2, ar] = cart2sph(a(1,:),a(2,:), a(3,:));
% 
% a = [a1/(2*pi);a2/pi]+1/2;
% epsilon = 0.4;
% a = (1 + epsilon)*a - epsilon/2;

sigma = 0.04;
data = f(X_grid,Y_grid,a,sigma);

% figure
% for i=1:250
% subplot(10, 25,i)
% imshow(reshape(data(:,4*i), n_grid ,n_grid))
% end

data = [data, f(X_grid,Y_grid,[.5 .5]',sigma)*linspace(0,1,100)];
%%
addpath(genpath('C:\Users\joperea\Documents\MATLAB\HLLE'))

[Y_hlle,~] = HLLE(data, 20, 3);

figure, plot3(Y_hlle(1,:), Y_hlle(2,:) , Y_hlle(3,:),'.')

grid on

%%
addpath(genpath('C:\Users\joperea\Documents\MATLAB\Isomap'))

distMat = squareform(pdist(data'));


[Y, R, E] = IsomapII(distMat, 'k', 20 );

%%
y  = Y.coords{end};

figure, plot3(y(1,:) , y(2,:), y(3,:), '.')

geo_distMat = squareform(pdist(y'));

diam = max(max(geo_distMat));
%%
addpath(genpath('C:\Users\joperea\Documents\MATLAB\Projective Coordinates'))

L = px_maxmin(geo_distMat,'metric',500,'n');

%%

figure
for i=1:70
subplot(7, 10,i)
imshow(reshape(data(:,L(i)), n_grid ,n_grid))
end

%%
addpath(genpath('C:\Users\joperea\Documents\MATLAB\Ripser'))
dgms  = ripserDM(geo_distMat(L,L),2,2);

figure, 
plotDGM(dgms{1})
hold on
plotDGM(dgms{2}, 'r')
plotDGM(dgms{3}, 'g')
axis square
legend(' ', 'dgm_0', '', 'dgm_1',  '', 'dgm_2','Location' , 'southeast')
% plotBarcodes(dgm{1})
% xlim([0 diam])
% figure, plotBarcodes(dgm{2})
% xlim([0 diam])
% figure, plotBarcodes(dgm{3})
% xlim([0 diam])

%%
dgm0 = dgms{1};
dgm1 = dgms{2};
dgm2 = dgms{3};

t = linspace(0,12, 120);

beta0 = zeros(size(t));
beta1 = zeros(size(t));
beta2 = zeros(size(t));

chi = beta0;

for i=1:length(t)
    chi(i) = sum( (dgm0(:,1) <= t(i)).*(t(i) < dgm0(:,2)) ) - sum( (dgm1(:,1) <= t(i)).*(t(i) < dgm1(:,2)) ) + sum( (dgm2(:,1) <= t(i)).*(t(i) < dgm2(:,2)));
end
figure, hist(categorical(chi))
xlim([0 37])


%%
n = 10;
[x, y] = meshgrid(linspace(0,1,n), linspace(0,1,n)) ;
y = flipud(y) ;
x = reshape(x',[],1);
y = reshape(y',[],1);
a = [x'; y'];
epsilon = 0.4;
a = (1 + epsilon)*a - epsilon/2;
sigma = 0.02;
model_data = f(X_grid,Y_grid,a,sigma);


figure
for i=1:25
subplot(5, 5 ,i)
imshow(reshape(model_data(:, i), n_grid ,n_grid))
end


