﻿All data is stored in Matlab data (.mat) files that were created with Matlab 2013a

Contents:


1) Moving_grating_spike_counts_xxxxx_xx.mat

Naming convention: the 'xxxxx' is the animal identifier (e.g. 'ma025'), and the last 'xx' 
		is either 'ip' for stimulus presented to ipsilateral eye, or 'co' for 
		stimulus presented to contralateral eye.

Variables:

- spontaneous_spike_count: a N x 180 x 351 matrix, with the spike counts in each 2ms time
		bin, for N electrodes, 180 trials where the stimulus was a blank screen, 
		and 351 time bins. N is the number of electrodes identified as within MT 
		or MTc.

- trial_spike_count: a N x 2160 x 351 matrix, with the spike counts in each 2ms time
		bin, for N electrodes, 2160 stimulus trials and 351 time bins.

- trial_stimulus: a structure array specifying the stimulus for each of the 2160 stimulus
		trials in the matrix 'trial_spike_count'. The direction (degrees from 
		downwards), spatial frequency (cycles/deg), and temporal frequency (Hz) 
		are given by the fields 'grating_direction', 'grating_sf' and 'grating_tf').

- time_ms: the time (ms) relative to stimulus onset, for the 351 time points in the 
		trial_spike_count and spontaneous_spike_count matrices.


2) Moving_grating_classification_performance.mat

Index into datasets: in this file the data from all datasets is in a single matrix. The 6 
		datasets (in order) are: 'ma025_co', 'ma025_ip', 'ma026_ip', 'ma026_co', 
		'ma027_ip', 'my147_ip'.

Variables:

- classifierAccuracy: a 6 x 351 x 108 x 108 matrix of classifier accuracies (proportion 
		correct), for 6 datasets (see above) at 351 time points, where classifiers 
		were trained to discriminate each of the 108 stimulus exemplars from each 
		other exemplar.

- axisLabels: a structure array giving the axis labels for the classifierAccuracy matrix.
		The direction (degrees from downwards), spatial frequency (cycles/deg) 
		and temporal frequency (Hz) are given for the 108 exemplars (fields 
		'grating_direction', 'grating_sf' and 'grating_tf').
		The field 'time_ms' gives the time (ms) relative to stimulus onset, for
		the 351 time points in the classifierAccuracy matrix



3) Moving_dot_field_spike_counts_xxxxx_xx.mat

Naming convention: the 'xxxxx' is the animal identifier (e.g. 'ma025'), and the last 'xx' 
		is either 'ip' for stimulus presented to ipsilateral eye, or 'co' for 
		stimulus presented to contralateral eye.

Variables:

- spontaneous_spike_count: a N x 140 x 351 matrix, with the spike counts in each 2ms time
		bin, for N electrodes, 140 trials where the stimulus was a blank screen, 
		and 351 time bins. N is the number of electrodes identified as within MT 
		or MTc.

- trial_spike_count: a N x 1680 x 351 matrix, with the spike counts in each 2ms time
		bin, for N electrodes, 1680 stimulus trials and 351 time bins.

- trial_stimulus: a structure array specifying the stimulus for each of the 1680 stimulus
		trials in the matrix 'trial_spike_count'. The direction (degrees from 
		downwards) and speed (deg/s) are given by the fields 'dot_field_direction' 
		and 'dot_field_speed').

- time_ms: the time (ms) relative to stimulus onset, for the 351 time points in the 
		trial_spike_count and spontaneous_spike_count matrices.


4) Moving_dot_field_classification_performance.mat

Index into datasets: in this file the data from all datasets is in a single matrix. The 6 
		datasets (in order) are: 'ma025_co','ma025_ip', 'ma026_ip', 'ma027_ip', 
		'my145_ip', 'my147_ip'

Variables:

- classifierAccuracy: a 6 x 351 x 84 x 84 matrix of classifier accuracies (proportion correct), 
		for 6 datasets (see above) at 351 time points, where classifiers were 
		trained to discriminate each of the 84 stimulus exemplars from each 
		other exemplar.

- axisLabels: a structure array giving the axis labels for the classifierAccuracy matrix.
		The direction (degrees from downwards) and speed (deg/s) are given by 
		the fields 'dot_field_direction' and 'dot_field_speed').
		The field 'time_ms' gives the time (ms) relative to stimulus onset, for
		the 351 time points in the classifierAccuracy matrix
