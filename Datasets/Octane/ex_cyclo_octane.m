% Input: 
%   data    :   a matrix of size d_data - by - n_data; data points are columns
%   landmarks   :   a matrix of size d_data - by - (s+1); landamarks are columns
%   cocycle   :   a matrix of size c x 2; each row is a non-zero edge

disp('Setting data and cocycle...')

load('data_example_cyclo-octane.mat')

data = [X_sample, landmarks];

clearvars X_sample


% get dimensions 

[d_data, n_data] = size(data);
s = size(landmarks,2)-1;
c = size(cocycle,1);



%% Run Isomap, Nearest neighboor graph with n_k = 7
disp('Running Isomap ...')

% % Run Isomap
% % Total distance matrix
% 
% distMat = squareform(pdist(data'));
% 
% addpath(genpath('C:\Users\joperea\Documents\MATLAB\Isomap'))
% n_k = 7;
% options.dims = 1:size(data,1);
% % options.verbose = 0;
% data_iso = Isomap(distMat,'k',n_k,options);


%% Plot isomap reconstruction

coord_Isomap = data_iso.coords{4};

alpha = coord_Isomap(1,:);
beta = coord_Isomap(2,:);
gamma = coord_Isomap(3,:);
eta = coord_Isomap(4,:);

figure
scatter3(alpha, beta, gamma , 25,eta,'.');
grid on
axis square
axs = gca;
colormap(axs, 'jet')
title('Isomap coordinates - color: 4-th isomap')


%% Ripser persistence computation on landmarks
disp('Computing Persistent Cohomology ...')
% addpath(genpath('C:\Users\joperea\Documents\MATLAB\Ripser'))
% 
distMat = data_iso.distMat;
dist_land_data = data_iso.distMat((end-s):end, :);
dist_land_land = data_iso.distMat((end-s):end,(end-s):end);
% 
% dgms = ripserDM(dist_land_land, 2,2);


%% Plot persistence diagrams dimesions 0, 1 and 2 

figure
plotDGM(dgms{1})
h_dgm1 = gcf;
hold on
plotDGM(dgms{2}, 'r')
h_dgm2 = gcf;
plotDGM(dgms{3}, 'g')
h_dgm3 = gcf;

legend(' ', 'dgm_0', '', 'dgm_1','', 'dgm_2', 'Location' , 'southeast')

%% Determine radius for balls ( = interpolant btw data coverage and cohomological birth)

dgm1 = dgms{2};

[~, ind_mp1] = max(dgm1(:,2) - dgm1(:,1));

coverage = max(min(dist_land_data));

perc = 0.99;

r_birth = (1 - perc)*max(dgm1(ind_mp1,1), coverage) + perc*dgm1(ind_mp1,2);

%% Create the open covering U = {U_1,..., U_{s+1}} and partition of unity
disp('Computing classifying map...')

% Let U_j be the set of data points whose distance to l_j is less than
% r_birth
U = (dist_land_data < repmat(r_birth*ones(s+1,1),1,n_data));

% Compute subordinated partition of unity varphi_1,...,varphi_{s+1}
% Compute the bump phi_j(b) on each data point b in U_j. phi_j = 0 outside U_j.

phi = zeros(s+1,n_data);

for j=1:(s+1)
     phi(j,U(j,:)) =   r_birth - dist_land_data(j, U(j,:));
%      phi(j,U(j,:)) =   (r_birth - dist_land_data(j, U(j,:))).^2;
%      phi(j,U(j,:)) = exp(r_birth^2./( dist_land_data(j, U(j,:)).^2 - r_birth^2 ));
end

% Compute the partition of unity varphi_j(b) = phi_j(b)/(phi_1(b) + ... + phi_{s+1}(b))

varphi = phi./(ones(s+1,1)*sum(phi));

% To each data point, associate the index of the first open set it belongs to

indx = zeros(1,n_data);

for i=1:n_data
    indx(i) = find(U(:,i),1,'first');
end

% From U_1 to U_{s+1} - (U_1 \cup ... \cup U_s), apply classifying map

% compute all transition functions

cocycle_matrix = ones(s+1);
cocycle_matrix((s+1)*(cocycle(:,2)-1)+cocycle(:,1)) = -1;
cocycle_matrix = cocycle_matrix'.*cocycle_matrix;

class_map = sqrt(varphi');

for i=1:n_data
    class_map(i,:) = class_map(i,:).*cocycle_matrix(indx(i),:);
end

%% Run principal projective component analysis
disp('Applying principal projective coordinates...')
X = class_map.';
variance = zeros(1,size(X,1)-1);
proj_dim = 3; % dimension of projective space onto which to project.

% Projective dimensionality reduction : Main Loop
n_iter = size(class_map,2)-proj_dim-1;
tic
for i=1: n_iter
    if toc > 7
        disp([' ... Iteration ' , num2str(i), ' of ', num2str(n_iter+proj_dim+1)])
        tic
    end
    [UU, ~, ~] = svd(X, 'econ');
    variance(end+1-i) = mean((pi/2 - real(acos(abs(UU(:,end)'*X))) ).^2) ;
    Y = UU'*X;
    y = Y(end,:);
    Y(end,:)= [];
    X = Y./(ones(size(Y,1),1)*sqrt(1 - abs(y).^2));   
end

Z = X;

for j=1:proj_dim
    [UU, ~, ~] = svd(Z, 'econ');
    variance(proj_dim-j+1) = mean((pi/2 - real(acos(abs(UU(:,end)'*Z))) ).^2) ;
    Y = UU'*Z;
    y = Y(end,:);
    Y(end,:)= [];
    Z = Y./(ones(size(Y,1),1)*sqrt(1 - abs(y).^2));
end
% 
% % Compute Frechet mean and variance 
% 
% S1toRadian = atan(Z(2,:)./Z(1,:));
% meantheta = geodmeanS1(S1toRadian');
% % variance(1) = mean((mod(S1toRadian - meantheta + pi,2*pi)-pi).^2);
% variance(1) = mean((real(acos(abs([cos(meantheta) sin(meantheta)]*Z))) ).^2) ;

% compute cumulative variance

variance = cumsum(variance);
variance = variance/variance(end);
figure, plot(variance, '--*'), ylim([0 1])
title('Profile or recovered variance')
xlabel('Projective embedding dimension')
ylabel('Percentage of cummulative variance')

%%
% addpath(genpath('C:\Users\joperea\Documents\MATLAB\Projective Coordinates'))
% rotate by singular component %% This!!
[UU, ~ ,~] = svd(X, 'econ');
X = rotMat(UU(:,end))*X;

% % rotate by principal component
% UU = pca(X');
% X = rotMat(UU(:,end))*X;

ind = (X(end,:) < 0);
X(:,ind) = - X(:,ind);

%% Plot data and RP^3 coordinates

figure

% plot data

subplot(2,3,1); 
scatter3(alpha, beta, gamma , 25,alpha,'.'), grid on
axis square
axs = gca;
colormap(axs, 'jet')
title('$$Cyclo-octane$$', 'interpreter', 'latex')


subplot(2,3,4); 
scatter3(alpha, beta, gamma , 25,beta,'.'), grid on
axis square
axs = gca;
colormap(axs, 'jet')
title('$$Cyclo-octane$$', 'interpreter', 'latex')


% plot RP^3 coordinates
% stereographic projection first:
X_stereo = X(1:(end-1),:)./(ones(size(X,1)-1,1)*(1 + X(end,:)));

subplot(2,3,2);
scatter3(X_stereo(1,:) , X_stereo(2,:) ,  X_stereo(3,:), 25, alpha,'.'); xlim([-1 1]), ylim([-1 1]), zlim([-1 1])
axis square   
axs = gca;
colormap(axs, 'jet')
hold on 
[x_sphere, y_sphere, z_sphere] = sphere;
surf(x_sphere, y_sphere, z_sphere,'FaceAlpha',0, 'EdgeColor', [.8 .8 .8])
title('$$R\mathbf{P}^3-\mbox{coordinates}$$','interpreter','latex')


subplot(2,3,5); 
scatter3(X_stereo(1,:) , X_stereo(2,:),  X_stereo(3,:), 25, beta,'.'), xlim([-1 1]), ylim([-1 1]), zlim([-1 1])

axis square 
axs = gca;
colormap(axs, 'jet')
hold on 
[x_sphere, y_sphere, z_sphere] = sphere;
surf(x_sphere, y_sphere, z_sphere,'FaceAlpha',0, 'EdgeColor', [.8 .8 .8])
title('$$R\mathbf{P}^3-\mbox{coordinates}$$','interpreter','latex')

%% Plot RP^2 coordinates
[UU, ~, ~] = svd(X, 'econ');
Y = UU'*X;
y = Y(end,:);
Y(end,:)= [];
XX = Y./(ones(size(Y,1),1)*sqrt(1 - abs(y).^2));

% rotate by main singular vector
[UU, ~ ,~] = svd(XX, 'econ');
XX = rotMat(UU(:,1))*XX;


ind = (XX(end,:) < 0);
XX(:,ind) = - XX(:,ind);

XX_stereo = XX(1:(end-1),:)./(ones(size(XX,1)-1,1)*(1 + XX(end,:)));

subplot(2,3,3);
scatter(XX_stereo(1,:), XX_stereo(2,:),  25, alpha,'.'); xlim([-1 1]), ylim([-1 1])
axs = gca;
colormap(axs, 'jet')
hold on
h_circle =  ezplot('x^2 + y^2  -1', [-1 1]); set(h_circle, 'Color',[.8 .8 .8], 'LineWidth', 2.0)
xlim([-1 1]), ylim([-1 1])
title('')
axis square  
title('$$R\mathbf{P}^2-\mbox{coordinates}$$','interpreter','latex')

subplot(2,3,6); 
scatter(XX(1,:)./(1 + XX(3,:)), XX(2,:)./(1 + XX(3,:)), 25, beta,'.'), xlim([-1 1]), ylim([-1 1])
axs = gca;
colormap(axs, 'jet')
hold on
h_circle =  ezplot('x^2 + y^2  -1', [-1 1]); set(h_circle, 'Color',[.8 .8 .8], 'LineWidth', 2.0)
xlim([-1 1]), ylim([-1 1])
title('')
axis square 
title('$$R\mathbf{P}^2-\mbox{coordinates}$$','interpreter','latex')

 